"! <p class="shorttext synchronized" lang="de">CX for ZCL_BAL</p>
class ZCX_BAL definition
  public
  inheriting from CX_STATIC_CHECK
  final
  create public .

public section.   "###################################################

  interfaces IF_T100_DYN_MSG .
  interfaces IF_T100_MESSAGE .

      "! Constructor
      "! @parameter iv_textid | Text-ID
      "! @parameter iv_previous | Previous message
  methods CONSTRUCTOR
    importing
      !TEXTID like IF_T100_MESSAGE=>T100KEY optional
      !PREVIOUS like PREVIOUS optional .
      "! Returns the message which is raised
      "! @parameter rv_msg | Raised message
  methods GET_MSG_IN_BAL_STRUCTURE
    returning
      value(RV_MSG) type BAL_S_MSG .
      "! Returns the message id
      "! @parameter rv_msgid | Message id
  methods GET_MSGID
    returning
      value(RV_MSGID) type SYMSGID .
      "! Returns the message number
      "! @parameter rv_msgno | Message number
  methods GET_MSGNO
    returning
      value(RV_MSGNO) type SYMSGNO .
      "! Returns the message type
      "! @parameter rv_msgty | Message type
  methods GET_MSGTY
    returning
      value(RV_MSGTY) type SYMSGTY .
      "! Returns the first message variable
      "! @parameter rv_msgv1 | First message variable
  methods GET_MSGV1
    returning
      value(RV_MSGV1) type SYMSGV .
      "! Returns the second message variable
      "! @parameter rv_msgv2 | Second message variable
  methods GET_MSGV2
    returning
      value(RV_MSGV2) type SYMSGV .
      "! Returns the third message variable
      "! @parameter rv_msgv3 | Third message variable
  methods GET_MSGV3
    returning
      value(RV_MSGV3) type SYMSGV .
      "! Returns the fourth message variable
      "! @parameter rv_msgv4 | Fourth message variable
  methods GET_MSGV4
    returning
      value(RV_MSGV4) type SYMSGV .
      "! Sends the message via MESSAGE ID id TYPE type NUMBER
      "! number WITH msgv1 msgv2 msgv3 msgv4 to the GUI.
  methods SEND_MESSAGE_TO_GUI .
      "! Sends the message to SY
  methods SEND_MESSAGE_TO_SY .
  PROTECTED SECTION. "################################################

  PRIVATE SECTION. "##################################################

ENDCLASS.



CLASS ZCX_BAL IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
PREVIOUS = PREVIOUS
.
clear me->textid.
if textid is initial.
  IF_T100_MESSAGE~T100KEY = IF_T100_MESSAGE=>DEFAULT_TEXTID.
else.
  IF_T100_MESSAGE~T100KEY = TEXTID.
endif.
  endmethod.


  METHOD GET_MSGID.
    CLEAR: rv_msgid.
    rv_msgid = if_t100_message~t100key-msgid.
  ENDMETHOD.


  METHOD GET_MSGNO.
    CLEAR: rv_msgno.
    rv_msgno = if_t100_message~t100key-msgno.
  ENDMETHOD.


  METHOD GET_MSGTY.
    CLEAR: rv_msgty.
    rv_msgty = if_t100_dyn_msg~msgty.
  ENDMETHOD.


  METHOD GET_MSGV1.
    CLEAR: rv_msgv1.
    rv_msgv1 = if_t100_dyn_msg~msgv1.
  ENDMETHOD.


  METHOD GET_MSGV2.
    CLEAR: rv_msgv2.
    rv_msgv2 = if_t100_dyn_msg~msgv2.
  ENDMETHOD.


  METHOD GET_MSGV3.
    CLEAR: rv_msgv3.
    rv_msgv3 = if_t100_dyn_msg~msgv3.
  ENDMETHOD.


  METHOD GET_MSGV4.
    CLEAR: rv_msgv4.
    rv_msgv4 = if_t100_dyn_msg~msgv4.
  ENDMETHOD.


  METHOD GET_MSG_IN_BAL_STRUCTURE.

    CLEAR: rv_msg.

* If no message is entered, then use a general message
    IF if_t100_message~t100key-msgid IS INITIAL
     OR if_t100_dyn_msg~msgty IS INITIAL.

      rv_msg-msgid = 'POC_MAIN'.
      rv_msg-msgno = '001'.
      rv_msg-msgty = 'W'.
      rv_msg-msgv1 = |NO MESSAGE: ( TCODE { sy-tcode } CALLED FROM { sy-cprog } )|.
    ENDIF.

* Otherwise use the message which caused this exception
    rv_msg-msgid = if_t100_message~t100key-msgid.
    rv_msg-msgty = if_t100_dyn_msg~msgty.
    rv_msg-msgno = if_t100_message~t100key-msgno.
    rv_msg-msgv1 = if_t100_dyn_msg~msgv1.
    rv_msg-msgv2 = if_t100_dyn_msg~msgv2.
    rv_msg-msgv3 = if_t100_dyn_msg~msgv3.
    rv_msg-msgv4 = if_t100_dyn_msg~msgv4.

  ENDMETHOD.


  METHOD SEND_MESSAGE_TO_GUI.

* If no message is entered, then use a general message
    IF if_t100_message~t100key-msgid IS INITIAL
     OR if_t100_dyn_msg~msgty IS INITIAL.
      MESSAGE w001(poc_main) WITH |NO MESSAGE: ( TCODE { sy-tcode } CALLED FROM { sy-cprog } )|.
    ENDIF.

* Otherwise send the message which caused this exception
    MESSAGE ID if_t100_message~t100key-msgid
        TYPE if_t100_dyn_msg~msgty
        NUMBER if_t100_message~t100key-msgno
        WITH if_t100_dyn_msg~msgv1
            if_t100_dyn_msg~msgv2
            if_t100_dyn_msg~msgv3
            if_t100_dyn_msg~msgv4.

  ENDMETHOD.


  METHOD SEND_MESSAGE_TO_SY.

    MESSAGE ID if_t100_message~t100key-msgid
      TYPE if_t100_dyn_msg~msgty
      NUMBER if_t100_message~t100key-msgno
      WITH if_t100_dyn_msg~msgv1
          if_t100_dyn_msg~msgv2
          if_t100_dyn_msg~msgv3
          if_t100_dyn_msg~msgv4
      INTO DATA(lv_msg).

  ENDMETHOD.
ENDCLASS.
