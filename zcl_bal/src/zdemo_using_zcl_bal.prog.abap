*&---------------------------------------------------------------------*
*& Report ZDEMO_USING_ZCL_BAL
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zdemo_using_zcl_bal.

DATA: go_cx_bal    TYPE REF TO zcx_bal,
      gv_msg       TYPE string,
      gs_bal_s_msg TYPE bal_s_msg,
      gt_bal_t_log TYPE TABLE OF bal_s_msg.

*============== CREATE BAL OBJECT
*TRY.

DATA(go_bal) = NEW zcl_bal( iv_object = zcl_bal=>cs_object-test
                            iv_subobject = zcl_bal=>cs_subobject-test
                            iv_extnumber = |'TEST' && { sy-sysid } { sy-datum } etc.|
                            iv_deldate = |20210820|
                          ).

*============== ADD MSG TO BAL

** VIA SY
MESSAGE s053(start_messages) WITH |EXAMPLE SUCCESS MESSAGE| INTO gv_msg.
go_bal->add_msg_with_symsg( ).

** VIA SINGLE MSG FIELDS
go_bal->add_msg_with_single_fields(
  EXPORTING
    iv_msgty = |I|
    iv_msgid = |START_MESSAGES|
    iv_msgno = |053|
    iv_msgv1 = |EXAMPLE INFO MESSAGE|
*   iv_msgv2 =
*   iv_msgv3 =
*   iv_msgv4 =
).

** VIA BALMSG, BAPIRET2 AND RECAMSG
gs_bal_s_msg-msgid = |START_MESSAGES|.
gs_bal_s_msg-msgno = |053|.
gs_bal_s_msg-msgty = |W|.
gs_bal_s_msg-msgv1 = |EXAMPLE WARNING MESSAGE|.

go_bal->add_msg_with_balmsg(
  EXPORTING
*   it_bal_t_msg =
    is_bal_s_msg = gs_bal_s_msg
).

** VIA EXCEPTION
TRY.
    MESSAGE e053(start_messages) WITH |EXAMPLE ERROR MESSAGE| INTO gv_msg.
    RAISE EXCEPTION TYPE zcx_bal USING MESSAGE.
  CATCH zcx_bal INTO go_cx_bal.
    "If object is already created
    go_bal->add_msg_with_exception( io_exception = go_cx_bal ).
    "If msg collecting via interal itab
*    zcl_bal=>add_exc_msg_to_itab( EXPORTING io_exception = go_cx_bal
*                                   CHANGING xt_bal_t_msg = gt_bal_t_log ).
ENDTRY.

** VIA SY WITH STATIC METHOD IN LOCAL LOG WITHOUT CREATING BEFORE BAL OBJECT
MESSAGE a053(start_messages) WITH |EXAMPLE ABORT MESSAGE| INTO gv_msg.
zcl_bal=>add_sy_msg_to_itab(
  CHANGING
*   xt_recamsg   =
    xt_bal_t_msg = gt_bal_t_log
*   xt_bapiret2  =
).

*============== DISPLAYING AND SAVING

* SHOW MESSAGES WITHOUT SAVING
TRY.
    go_bal->add_msg_with_balmsg(
      EXPORTING
        it_bal_t_msg = gt_bal_t_log
*       is_bal_s_msg =
    ).
    go_bal->display( iv_single = abap_true ).
  CATCH zcx_bal INTO go_cx_bal.
    MESSAGE w001(poc_main) WITH |DISPLAY OF BAL NOT POSSSIBLE| INTO gv_msg.
    go_cx_bal->send_message_to_gui( ).
ENDTRY.

** SHOW MESSAGES AFTER SAVING
*TRY.
*    go_bal->store_display_free( ).
*  CATCH zcx_bal INTO go_cx_bal.
*    MESSAGE w001(poc_main) WITH |DISPLAY OF BAL NOT POSSSIBLE| INTO gv_msg.
*    go_cx_bal->send_message_to_gui( ).
*ENDTRY.

** SAVE ONLY
*TRY.
*    go_bal->store_and_free( ).
*  CATCH zcx_bal INTO go_cx_bal.
*    MESSAGE w001(poc_main) WITH |DISPLAY OF BAL NOT POSSSIBLE| INTO gv_msg.
*    go_cx_bal->send_message_to_gui( ).
*ENDTRY.

**============== GETTER
*DATA(go_msgs_bapiret2) = go_bal->get_all_msgs_bapiret2( ).
*DATA(go_msgs_with_msgty_e) = go_bal->get_msgs_from_msgty_recamsg( iv_msgty = 'E' ).
*DATA(go_msgs_with_msgty_e_or_higher) = go_bal->get_msgs_from_msgty_recamsg( iv_msgty = 'E' iv_or_higher = abap_true ).
*DATA(go_stats) = go_bal->get_statistics( ).
*
**============== CHECKS
*IF go_bal->is_empty( ).
*  "yes
*ELSE.
*  "no
*ENDIF.
*
**============== DELETING
*go_bal->delete_all_collected_msgs( ).
*MESSAGE s053(start_messages) WITH |ALL MSGS DELETED| INTO gv_msg.
*go_bal->add_msg_with_symsg( ).
*
*** SHOW MESSAGES WITHOUT SAVING
*TRY.
*    go_bal->display( iv_single = abap_true ).
*  CATCH zcx_bal INTO go_cx_bal.
*    MESSAGE w001(poc_main) WITH |DISPLAY OF BAL NOT POSSIBLE| INTO gv_msg.
*    go_cx_bal->send_message_to_gui( ).
*ENDTRY.
