class ZCL_BAL definition
  public
  final
  create public .

public section.

  types:
    TT_RECAMSG type table of RECAMSG .

  constants:
    BEGIN OF cs_object,
        test  TYPE balobj_d VALUE 'TEST_BRF_LOG' ##NO_TEXT,
      END OF cs_object .
  constants:
    BEGIN OF cs_subobject,
       test      TYPE balsubobj VALUE 'TEST_BRF_LOG' ##NO_TEXT,
      END OF cs_subobject .

  methods CONSTRUCTOR
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
      !IV_EXTNUMBER type BALNREXT
      !IV_DELDATE type RECADATEFROM optional .
  methods ADD_MSG_WITH_SINGLE_FIELDS
    importing
      !IV_MSGTY type SYMSGTY
      !IV_MSGID type SYMSGID
      !IV_MSGNO type SYMSGNO
      !IV_MSGV1 type CLIKE optional
      !IV_MSGV2 type CLIKE optional
      !IV_MSGV3 type CLIKE optional
      !IV_MSGV4 type CLIKE optional .
  methods ADD_MSG_WITH_RECAMSG
    importing
      !IS_RECAMSG type RECAMSG
      !IT_RECAMSG type TT_RECAMSG .
  methods ADD_MSG_WITH_BAPIRET2
    importing
      !IT_BAPIRET type BAPIRETTAB optional
      !IS_BAPIRET type BAPIRET2 optional .
  methods ADD_MSG_WITH_BALMSG
    importing
      !IT_BAL_T_MSG type BAL_T_MSG optional
      !IS_BAL_S_MSG type BAL_S_MSG optional .
  methods ADD_MSG_WITH_SYMSG .
  methods ADD_MSG_WITH_EXCEPTION
    importing
      !IO_EXCEPTION type ref to CX_ROOT .
  methods GET_ALL_MSGS_BAPIRET2
    returning
      value(RT_BAPIRET2) type BAPIRETTAB .
  methods GET_MSGS_FROM_MSGTY_RECAMSG
    importing
      !IV_MSGTY type SYMSGTY
      !IV_OR_HIGHER type ABAP_BOOL default ABAP_FALSE
    returning
      value(RT_LIST) type RE_T_MSG .
  methods GET_STATISTICS
    returning
      value(RS_STATISTICS) type BAL_S_SCNT .
  methods GET_PROTOCOL_NO
    returning
      value(RV_PROTOCOL_NO) type BALOGNR .
  methods DELETE_ALL_COLLECTED_MSGS .
  methods IS_EMPTY
    returning
      value(RV_EMPTY) type ABAP_BOOL .
  methods DISPLAY
    importing
      value(IV_SINGLE) type CUFLAG1 optional
      value(IV_POPUP) type CUFLAG1 optional
      value(IV_GRID) type CUFLAG1 default ABAP_TRUE
    raising
      ZCX_BAL .
  methods STORE_AND_FREE
    raising
      ZCX_BAL .
  methods STORE_DISPLAY_FREE
    importing
      value(IV_SINGLE) type CUFLAG1 optional
      value(IV_POPUP) type CUFLAG1 optional
      value(IV_GRID) type CUFLAG1 default ABAP_TRUE
    raising
      ZCX_BAL .
  class-methods ADD_SY_MSG_TO_ITAB
    changing
      !XT_RECAMSG type TT_RECAMSG optional
      !XT_BAL_T_MSG type BAL_T_MSG optional
      !XT_BAPIRET2 type BAPIRET2_TT optional .
  class-methods ADD_EXC_MSG_TO_ITAB
    importing
      !IO_EXCEPTION type ref to CX_ROOT
    changing
      !XT_RECAMSG type TT_RECAMSG optional
      !XT_BAL_T_MSG type BAL_T_MSG optional
      !XT_BAPIRET2 type BAPIRET2_TT optional .
  class-methods FIND_BAL_PROTOCOL_BY_EXTNO
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
      !IV_EXTNUMBER type BALNREXT
      !IV_DELIVER_NEWEST type RECABOOL default ABAP_FALSE
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
  class-methods FIND_BAL_PROTOCOL_BY_HANDLE
    importing
      !IV_HANDLE type BALLOGHNDL
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
  class-methods FIND_BAL_LAST_STORED
    importing
      !IV_OBJECT type BALOBJ_D
      !IV_SUBOBJECT type BALSUBOBJ
    returning
      value(RO_INSTANCE) type ref to IF_RECA_MESSAGE_LIST
    exceptions
      ERROR .
protected section.
private section.

  data _GO_BAL type ref to IF_RECA_MESSAGE_LIST .
  data _GV_HANDLE type BALLOGHNDL .

  methods README .
ENDCLASS.



CLASS ZCL_BAL IMPLEMENTATION.

* <SIGNATURE>---------------------------------------------------------------------------------------+
* | Static Public Method ZCL_BAL=>ADD_MSG_FROM_EXC_TO_ITAB
* +-------------------------------------------------------------------------------------------------+
* | [--->] IO_EXCEPTION                   TYPE REF TO CX_ROOT
* | [<-->] XT_RECAMSG                     TYPE        TT_RECAMSG(optional)
* | [<-->] XT_BAL_T_MSG                   TYPE        BAL_T_MSG(optional)
* | [<-->] XT_BAPIRET2                    TYPE        BAPIRET2_TT(optional)
* +--------------------------------------------------------------------------------------</SIGNATURE>
METHOD add_exc_msg_to_itab.

  DATA: ld_text   TYPE string.

* PRECONDITIONS
  ASSERT io_exception IS BOUND.

* BODY
* get short text of exception object
  ld_text = io_exception->get_text( ).

* use generic message
  CALL METHOD cl_reca_string_services=>raise_string_as_symsg
    EXPORTING
      id_string = ld_text
      id_msgty  = 'E'
    EXCEPTIONS
      message   = 1
      OTHERS    = 2.
  IF sy-subrc <> 0.
    IF xt_recamsg IS SUPPLIED.
      APPEND INITIAL LINE TO xt_recamsg ASSIGNING FIELD-SYMBOL(<ls_recamsg>).
      MOVE-CORRESPONDING sy TO <ls_recamsg>.
    ENDIF.

    IF xt_bal_t_msg IS SUPPLIED.
      APPEND INITIAL LINE TO xt_bal_t_msg  ASSIGNING FIELD-SYMBOL(<ls_bal_msg>).
      MOVE-CORRESPONDING sy TO <ls_bal_msg>.
    ENDIF.

    IF xt_bapiret2 IS SUPPLIED.
      APPEND INITIAL LINE TO xt_bapiret2 ASSIGNING FIELD-SYMBOL(<ls_bapi_msg>).
      MOVE-CORRESPONDING sy TO <ls_bapi_msg>.
    ENDIF.

  ENDIF.

ENDMETHOD.

METHOD add_msg_with_balmsg.

  DATA: ls_recamsg TYPE recamsg.

  IF it_bal_t_msg IS NOT INITIAL.
    LOOP AT it_bal_t_msg ASSIGNING FIELD-SYMBOL(<ls_bal_msg>).
      MOVE-CORRESPONDING <ls_bal_msg> TO ls_recamsg.
      me->_go_bal->add(
        EXPORTING
          is_message       = ls_recamsg
        IMPORTING
          es_message       = DATA(ls_message_t)
      ).
      CLEAR ls_recamsg.
    ENDLOOP.
  ENDIF.

  IF is_bal_s_msg IS NOT INITIAL.
    CLEAR ls_recamsg.
    MOVE-CORRESPONDING is_bal_s_msg TO ls_recamsg.
    me->_go_bal->add(
      EXPORTING
        is_message       = ls_recamsg
      IMPORTING
        es_message       = DATA(ls_message_s)
    ).
  ENDIF.



ENDMETHOD.


METHOD add_msg_with_bapiret2.

  IF it_bapiret IS NOT INITIAL.
    me->_go_bal->add_from_bapi(
   EXPORTING
     it_bapiret     = it_bapiret
     if_cumulate    = abap_false
   IMPORTING
     ef_add_error   = DATA(lv_add_error_tab)
     ef_add_warning = DATA(lv_add_warning_tab)
 ).
  ENDIF.

  IF is_bapiret IS NOT INITIAL.
    me->_go_bal->add_from_bapi(
      EXPORTING
        is_bapiret     = is_bapiret
        if_cumulate    = abap_false
      IMPORTING
        ef_add_error   = DATA(lv_add_error_str)
        ef_add_warning = DATA(lv_add_warning_str)
    ).
  ENDIF.

ENDMETHOD.


method ADD_MSG_WITH_EXCEPTION.

 me->_go_bal->add_from_exception(
   EXPORTING
     io_exception = io_exception
 ).

endmethod.


method ADD_MSG_WITH_RECAMSG.

  LOOP AT it_recamsg ASSIGNING FIELD-SYMBOL(<ls_recamsg>).
    _go_bal->add(
   EXPORTING
     is_message       = <ls_recamsg>
   IMPORTING
     es_message       = DATA(ls_msg)
 ).
  ENDLOOP.

 _go_bal->add(
   EXPORTING
     is_message       = is_recamsg
   IMPORTING
     es_message       = DATA(ls_message)
 ).

endmethod.


METHOD add_msg_with_single_fields.

  me->_go_bal->add( EXPORTING id_msgty = iv_msgty
                              id_msgid = iv_msgid
                              id_msgno = iv_msgno
                              id_msgv1 = iv_msgv1
                              id_msgv2 = iv_msgv2
                              id_msgv3 = iv_msgv3
                              id_msgv4 = iv_msgv4 ).

ENDMETHOD.


METHOD add_msg_with_symsg.

  DATA ls_symsg TYPE recasymsg.

  ls_symsg-msgty = sy-msgty.
  ls_symsg-msgid = sy-msgid.
  ls_symsg-msgno = sy-msgno.
  ls_symsg-msgv1 = sy-msgv1.
  ls_symsg-msgv2 = sy-msgv2.
  ls_symsg-msgv3 = sy-msgv3.
  ls_symsg-msgv4 = sy-msgv4.

  _go_bal->add(
    EXPORTING
      id_msgty     = ls_symsg-msgty
      id_msgid     = ls_symsg-msgid
      id_msgno     = ls_symsg-msgno
      id_msgv1     = ls_symsg-msgv1
      id_msgv2     = ls_symsg-msgv2
      id_msgv3     = ls_symsg-msgv3
      id_msgv4     = ls_symsg-msgv4
    IMPORTING
      es_message   = DATA(ls_message)
  ).

ENDMETHOD.


METHOD constructor.

  TRY.

      SELECT SINGLE * FROM balsub INTO @DATA(ls_exist_slg0_objects) WHERE object = @iv_object
                                                                      AND subobject = @iv_subobject.
      IF sy-subrc <> 0.
        MESSAGE e014(nr) WITH iv_subobject iv_object INTO DATA(lv_no_object).
        RAISE EXCEPTION TYPE zcx_bal USING MESSAGE.
      ENDIF.

      me->_go_bal = cf_reca_message_list=>create( EXPORTING id_object = iv_object
                                                            id_subobject = iv_subobject ).

      me->_go_bal->set_extnumber( EXPORTING id_extnumber = iv_extnumber ).

      DATA: lv_use_deldate_default TYPE recabool.
      lv_use_deldate_default = abap_true.
      IF iv_deldate IS NOT INITIAL.
        lv_use_deldate_default = abap_false.
        DATA(lv_deldate) = iv_deldate.
      ENDIF.
      me->_go_bal->set_expiration_date( EXPORTING if_use_default = lv_use_deldate_default
                                         CHANGING id_expiration_date = lv_deldate ).

    CATCH zcx_bal INTO DATA(lo_exp).
     lo_exp->send_message_to_gui( ).
  ENDTRY.

ENDMETHOD.


METHOD delete_all_collected_msgs.

  CHECK _go_bal->is_empty( ) = abap_false.
  _go_bal->clear( ).

ENDMETHOD.


  METHOD find_bal_last_stored.

    CLEAR ro_instance.

    cf_reca_message_list=>find_last_stored_log(
      EXPORTING
        id_object    = iv_object
        id_subobject = iv_subobject
      RECEIVING
        ro_instance  = ro_instance
      EXCEPTIONS
        error        = 1
        OTHERS       = 2
    ).
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDMETHOD.


METHOD find_bal_protocol_by_handle.

  CLEAR ro_instance.

  cf_reca_message_list=>find_by_handle(
    EXPORTING
      id_handle   = iv_handle
    receiving
      ro_instance = ro_instance
    EXCEPTIONS
      error       = 1
      OTHERS      = 2
  ).
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDMETHOD.


METHOD get_all_msgs_bapiret2.

 CLEAR rt_bapiret2.
 CHECK me->_go_bal->is_empty( ) = abap_false.

  _go_bal->get_list_as_bapiret(
    IMPORTING
      et_list = rt_bapiret2
  ).

ENDMETHOD.


METHOD get_msgs_from_msgty_recamsg.

  CLEAR rt_list.

  me->_go_bal->get_list(
    EXPORTING
      id_msgty     = iv_msgty
      if_or_higher = iv_or_higher
    IMPORTING
      et_list      = rt_list
  ).

ENDMETHOD.


METHOD get_statistics.

  CLEAR rs_statistics.

  rs_statistics = me->_go_bal->get_statistics( ).

ENDMETHOD.


METHOD is_empty.

  me->_go_bal->is_empty( ).

ENDMETHOD.


METHOD store_and_free.

  IF me->_go_bal->is_empty( ).
    RAISE EXCEPTION TYPE zcx_bal MESSAGE e100(WRMA).
  ENDIF.

  me->_go_bal->store( ).
  cf_reca_storable=>commit( if_wait = abap_true  ).

  DATA(ls_save_bal_header) = _go_bal->get_header( ).

  me->_gv_handle = _go_bal->get_handle( ).

  me->_go_bal->free( ).

  IF sy-batch = abap_true.
    MESSAGE s298(fsh_main) WITH ls_save_bal_header-extnumber
                                ls_save_bal_header-object
                                ls_save_bal_header-subobject.
  ENDIF.

ENDMETHOD.


METHOD display.

  "Copied from CL_CAI_APPL_LOG=>DISPLAY

  DATA: ls_display_profile TYPE bal_s_prof,
        lt_log_handle      TYPE bal_t_logh.

   IF me->_go_bal->count( ) = 0.
    MESSAGE w100(wrma) INTO DATA(lv_no_msgs_in_prot).
    me->_go_bal->add_symsg( ).
  ENDIF.

* get a prepared profile
  IF NOT iv_single IS INITIAL.
    CALL FUNCTION 'BAL_DSP_PROFILE_SINGLE_LOG_GET'
      IMPORTING
        e_s_display_profile = ls_display_profile.
  ELSEIF NOT iv_popup IS INITIAL.
    CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
      IMPORTING
        e_s_display_profile = ls_display_profile.
  ELSE.
    CALL FUNCTION 'BAL_DSP_PROFILE_STANDARD_GET'
      IMPORTING
        e_s_display_profile = ls_display_profile.
  ENDIF.

* use grid for display if wanted
  ls_display_profile-use_grid = iv_grid.

* set report to allow saving of variants
  ls_display_profile-disvariant-report = sy-repid.

* add_log_handle
  INSERT me->_go_bal->get_handle( ) INTO lt_log_handle INDEX 1.

  CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
    EXPORTING
      i_s_display_profile  = ls_display_profile
      i_t_log_handle       = lt_log_handle
    EXCEPTIONS
      profile_inconsistent = 1
      internal_error       = 2
      no_data_available    = 3
      no_authority         = 4
      OTHERS               = 5.

  IF sy-subrc = 3.
    MESSAGE e778(gc) INTO DATA(lv_no_data).
    RAISE EXCEPTION TYPE zcx_bal MESSAGE e778(gc).
  ENDIF.

  IF sy-subrc <> 0.
    RAISE EXCEPTION TYPE zcx_bal.
  ENDIF.
ENDMETHOD.


METHOD store_display_free.

  IF me->_go_bal->is_empty( ).
    RAISE EXCEPTION TYPE zcx_bal MESSAGE e100(WRMA).
  ENDIF.

  me->_go_bal->store( ).
  cf_reca_storable=>commit( if_wait = abap_true  ).

  me->display( ).

  DATA(ls_save_bal_header) = _go_bal->get_header( ).

  me->_gv_handle = _go_bal->get_handle( ).

  me->_go_bal->free( ).

  IF sy-batch = abap_true.
    MESSAGE s298(fsh_main) WITH ls_save_bal_header-extnumber
                                ls_save_bal_header-object
                                ls_save_bal_header-subobject.
  ENDIF.

ENDMETHOD.


method ADD_SY_MSG_TO_ITAB.

  IF xt_recamsg IS SUPPLIED.
  APPEND INITIAL LINE TO xt_recamsg ASSIGNING FIELD-SYMBOL(<ls_recamsg>).
  MOVE-CORRESPONDING sy TO <ls_recamsg>.
  ENDIF.

  IF xt_bal_t_msg IS SUPPLIED.
  APPEND INITIAL LINE TO xt_bal_t_msg  ASSIGNING FIELD-SYMBOL(<ls_bal_msg>).
  MOVE-CORRESPONDING sy TO <ls_bal_msg>.
  ENDIF.

  IF xt_bapiret2 IS SUPPLIED.
  APPEND INITIAL LINE TO xt_bapiret2 ASSIGNING FIELD-SYMBOL(<ls_bapi_msg>).
  MOVE-CORRESPONDING sy TO <ls_bapi_msg>.
  ENDIF.

endmethod.


method README.

*==== C R E A T E D  B Y
* Marvin Michel
* say-hi@m1ch3l.de
* wiki.m1ch3l.de
* version: 220426

*==== U S A G E IN COMMERCIAL AND PRIVAT ENVIRONMENT
* This z-class was created in freetime and NOT
* in a salaried activity.
* Because of this, the z-class
* can be used without any restriction.

*==== L I N K S
* EXPLANATION IN GERMAN: wiki.m1ch3l.de/ZCL_BAL
* DONWLOAD: gitlab.com/m1ch3l-de/abap-code-collection/-/tree/master/zcl_bal.

*==== C H A N G E L O G
*-------------------------------------------------------------------------
* yyyymmdd by fname lname:
*   Your changes here ;)
*-------------------------------------------------------------------------
endmethod.


METHOD FIND_BAL_PROTOCOL_BY_EXTNO.

  CLEAR ro_instance.

  cf_reca_message_list=>find(
    EXPORTING
      id_object         = iv_object
      id_subobject      = iv_subobject
      id_extnumber      = iv_extnumber
      if_deliver_newest = iv_deliver_newest
    RECEIVING
      ro_instance       = ro_instance
    EXCEPTIONS
      error             = 1
      OTHERS            = 2
  ).
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDMETHOD.


METHOD get_protocol_no.
 CLEAR rv_protocol_no.
 SELECT SINGLE lognumber FROM balhdr INTO rv_protocol_no WHERE log_handle = me->_gv_handle..
ENDMETHOD.
ENDCLASS.
