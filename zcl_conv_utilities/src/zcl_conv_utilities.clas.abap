class ZCL_CONV_UTILITIES definition
  public
  final
  create public .

public section.

  types:
    tt_tab_xml TYPE TABLE OF smum_xmltb .
  types:
    BEGIN OF ts_uom_iso_sap,
        iso_code TYPE t006-isocode,
        sap_code TYPE t006-msehi,
        unique   TYPE isofields-unique,
      END OF ts_uom_iso_sap .
  types:
    BEGIN OF ts_number_type,
        material TYPE String,
      END OF ts_number_type .

  constants:
    BEGIN OF gcs_number_type,
        material TYPE string VALUE 'MATN1',
      END OF gcs_number_type .
  constants GC_OUTPUT_TEXT type STRING value 'OUTPUT' ##NO_TEXT.
  constants GC_INPUT_TEXT type STRING value 'INPUT' ##NO_TEXT.
  constants GC_ALPHA_TEXT type STRING value 'ALPHA' ##NO_TEXT.
  class-data GS_UOM_ISO_SAP type TS_UOM_ISO_SAP .

  class-methods COL_TO_ROW_FORMAT_DYNAMIC
    importing
      !IT_COL_DATA type STRING_T
      !IV_STRUC_NAME type STRUCNAME
    returning
      value(RR_COL_TO_ROW) type ref to DATA .
  class-methods UOM_CONV_BETWEEN_SAP_ISO
    importing
      !IS_UOM type TS_UOM_ISO_SAP
    returning
      value(RS_UOM) type TS_UOM_ISO_SAP .
  class-methods XML_TO_ITAB
    importing
      !IV_XML_AS_UPLOAD type CHAR01 optional
      !IV_XML_FROM_APPLSERVER type LOCALFILE optional
    exporting
      value(ET_ITAB) type TT_TAB_XML .
  class-methods IDOC_TO_XML
    importing
      !IV_IDOC_NUMBER type EDI_DOCNUM
      !IV_SAVE_TO_DESKTOP type CHAR01 optional
      !IV_SAVE_TO_APPLSERVER type STRING optional
    returning
      value(RT_IDOCDATA_AS_XML) type STRINGTAB .
protected section.
private section.

  methods README .
ENDCLASS.



CLASS ZCL_CONV_UTILITIES IMPLEMENTATION.


  METHOD col_to_row_format_dynamic.

    DATA: lv_str_name     TYPE string,
          lv_line         TYPE REF TO data,
          lv_error_occurs TYPE abap_boolean,
          lr_structure    TYPE REF TO data.
    FIELD-SYMBOLS:  <structure> TYPE any.

    lv_str_name = iv_struc_name.
    CREATE DATA lv_line TYPE (lv_str_name).
    ASSIGN lv_line->* TO <structure>.

    LOOP AT it_col_data ASSIGNING FIELD-SYMBOL(<ls_col_data>).
      ASSIGN COMPONENT sy-tabix OF STRUCTURE <structure> TO FIELD-SYMBOL(<ls_field>).
      IF sy-subrc = 0.
        <ls_field> = <ls_col_data>.
      ELSE.
        RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e006(zmc_conv_utilities).
      ENDIF.
    ENDLOOP.

    GET REFERENCE OF <structure> INTO lr_structure.
    rr_col_to_row = lr_structure.

    "#!When calling this method:

    "The returnparameter should like this: DATA(build_struc) = this method(...).

    "The reference is assigned to a field symbol, where the method is identified and
    " e.g. a field symbol <tab> with TYPE STANDARD TABLE

    "DATA(lv_str_name) = 'ZZPO_HEADER'.
    "CREATE DATA lv_line TYPE TABLE OF (lv_str_name).
    "ASSIGN lv_line->* TO <tab>.

    "ASSIGN build_struc->* TO FIELD-SYMBOL(<structure>).
    "APPEND <structure> TO <tab>.

  ENDMETHOD.


METHOD idoc_to_xml.

      "Check IDoc No exists
      SELECT SINGLE docnum FROM EDIDC INTO @DATA(lv_idoc_exists) WHERE docnum = @iv_idoc_number.
      IF sy-subrc <> 0.
       RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e008(zmc_conv_utilities) WITH iv_idoc_number.
      ENDIF.

      DATA(lo_idoc_xml) = NEW cl_idoc_xml1( docnum = iv_idoc_number ).
      DATA lv_xml TYPE string.
      lo_idoc_xml->get_xmldata_as_string( IMPORTING data_string = lv_xml ).
      DATA(lt_xml) = VALUE stringtab( ( |{ lv_xml }| ) ).

      rt_idocdata_as_xml = lt_xml.

      IF iv_save_to_desktop = abap_true.
        DATA: lv_temp_dir TYPE string.
        cl_gui_frontend_services=>get_desktop_directory( CHANGING desktop_directory = lv_temp_dir ).
        cl_gui_cfw=>flush( ).
        DATA(lv_filename) = |{ lv_temp_dir }\\my_xml.xml|.
        cl_gui_frontend_services=>gui_download( EXPORTING
                                                  filename = lv_filename
                                                  filetype = 'ASC'
                                                CHANGING
                                                  data_tab = lt_xml ).

      ELSEIF iv_save_to_applserver IS NOT INITIAL.
        "Write file to application server
        cl_rsan_ut_appserv_file_writer=>appserver_file_write( EXPORTING i_filename = iv_save_to_applserver "max 255 char in AL11
                                                                        i_data_tab = lt_xml
                                                                        i_overwrite = abap_true
                                                              IMPORTING e_lines_written = DATA(lt_lines_written) ).
      ENDIF.

      "Use this method like this:
      "DATA(xml_tab) = zcl_conv_utilities=>idoc_to_xml( iv_idoc_number = p_idoc
      "                                                 iv_save_to_desktop ).

      "DATA(xml_tab) = zcl_conv_utilities=>idoc_to_xml( iv_idoc_number = p_idoc
      "                                                  iv_save_to_applserver = '/usr/sap/trans/xml2.xml' ).

      "DATA(xml_tab) = zcl_conv_utilities=>idoc_to_xml( iv_idoc_number = p_idoc ).
ENDMETHOD.


method README.

*==== C R E A T E D  B Y
* Marvin Michel
* say-hi@m1ch3l.de
* wiki.m1ch3l.de

*==== U S A G E IN COMMERCIAL AND PRIVAT ENVIRONMENT
* This z-class was created in freetime and NOT
* in a salaried activity.
* Because of this, the z-class
* can be used without any restriction.

*==== L I N K S
* EXPLANATION IN GERMAN: wiki.m1ch3l.de/ZCL_CONV_UTILITIES
* DONWLOAD: gitlab.com/m1ch3l-de/abap-code-collection/-/tree/master/zcl_conv_utilities

*==== C H A N G E L O G
*-------------------------------------------------------------------------
* yyyymmdd by fname lname:
*   Your changes here ;)
*-------------------------------------------------------------------------
endmethod.


 METHOD uom_conv_between_sap_iso.
   "#! This method combines both SAP fms for uom conversion.
   "#! Depending which fields are filled, the conversion will be done

   "#! Only fill one uom field in structure like:
   " For SAP -> ISO:
   "  Fill is_uom-sap_code and get rs_uom-iso_code
   " For ISO -> SAP:
   "  Fill is_uom-iso_code and optional is_uom-unique and get rs_uom-sap_code

   CHECK ( is_uom-iso_code IS NOT INITIAL AND is_uom-sap_code IS INITIAL )
      OR ( is_uom-iso_code IS INITIAL AND is_uom-sap_code IS NOT INITIAL ).

   DATA lv_subrc TYPE sy-subrc.

   "Conversion
   IF is_uom-sap_code IS NOT INITIAL.
     CALL FUNCTION 'UNIT_OF_MEASURE_SAP_TO_ISO'
       EXPORTING
         sap_code    = is_uom-sap_code
       IMPORTING
         iso_code    = rs_uom-iso_code
       EXCEPTIONS
         not_found   = 1
         no_iso_code = 2
         OTHERS      = 3.
     lv_subrc = sy-subrc.
   ELSE.
     CALL FUNCTION 'UNIT_OF_MEASURE_ISO_TO_SAP'
       EXPORTING
         iso_code  = is_uom-iso_code
       IMPORTING
         sap_code  = rs_uom-sap_code
         unique    = rs_uom-unique
       EXCEPTIONS
         not_found = 1
         OTHERS    = 2.
     lv_subrc = sy-subrc.
   ENDIF.

   "Error handling
   CHECK lv_subrc <> 0.
   CASE lv_subrc.
     WHEN 1.
       "NOT FOUND
       RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e003(zmc_conv_utilities).
     WHEN 2.
       IF is_uom-sap_code IS NOT INITIAL.
         "NO_ISO_CODE
         RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e004(zmc_conv_utilities).
       ELSE.
         "Unknown error
         RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e002(zmc_conv_utilities).
       ENDIF.
     WHEN 3.
       "Unknown error
       RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e002(zmc_conv_utilities).
     WHEN OTHERS.
       "Unknown error
       RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e002(zmc_conv_utilities).
   ENDCASE.

   "Use this method like:
   "uom-sap_code = 'ST'.
   "OR
   "uom-iso_code = 'PCE'.
   "uom = zcl_conv_utilities=>uom_conv_between_sap_iso( is_uom = uom ).
   "Note: Like this the source uom field is empty and the target uom is filled
   "Use global structure as type for defining a local structure
 ENDMETHOD.


METHOD xml_to_itab.
  TYPES tyt_tab_xml TYPE TABLE OF smum_xmltb.

  DATA: lv_xml_xstring  TYPE          xstring,
        lt_xml_tab      TYPE TABLE OF smum_xmltb,
        lv_filename     TYPE          localfile,
        lo_xml_document TYPE REF TO   cl_xml_document,
        lv_subrc        TYPE          sy-subrc,
        lv_size         TYPE          sy-tabix.

  CHECK iv_xml_as_upload = abap_true AND iv_xml_from_applserver IS INITIAL OR
        iv_xml_as_upload = abap_false AND iv_xml_from_applserver IS NOT INITIAL.

*== create the object
  CREATE OBJECT lo_xml_document.

*== XML from Upload
  IF iv_xml_as_upload = abap_true.
    DATA: lt_filetable     TYPE filetable,
          lv_anz_dat       TYPE i,
          lv_windows_title TYPE string.
    lv_windows_title = TEXT-001.

    cl_gui_frontend_services=>file_open_dialog(
      EXPORTING
        window_title            =     lv_windows_title
        default_filename        =     'C:\test.xml'
        initial_directory       =     'C:\'
    CHANGING
        file_table              = lt_filetable
        rc                      = lv_anz_dat
    EXCEPTIONS
      OTHERS                  = 5 ).

    READ TABLE lt_filetable INTO lv_filename INDEX 1.

*== Upload xml file
    lo_xml_document->import_from_file(
    EXPORTING
      filename = lv_filename
      RECEIVING
      retcode = lv_subrc  ).

    CHECK lv_subrc = 0.

*== Convert to xstring
    CALL METHOD lo_xml_document->render_2_xstring
      IMPORTING
        retcode = lv_subrc
        stream  = lv_xml_xstring
        size    = lv_size.

    CHECK lv_subrc = 0.

  ELSEIF iv_xml_from_applserver IS NOT INITIAL.

*== Read XML from applserver

    OPEN DATASET iv_xml_from_applserver FOR INPUT IN BINARY MODE.
    READ DATASET iv_xml_from_applserver INTO lv_xml_xstring.
    CLOSE DATASET iv_xml_from_applserver.

    CALL METHOD lo_xml_document->parse_xstring
      EXPORTING
        stream  = lv_xml_xstring
      RECEIVING
        retcode = lv_subrc.

    CHECK lv_subrc = 0.

  ENDIF.

*== Convert XML to itab

  DATA: lt_return TYPE TABLE OF bapiret2.

  CALL FUNCTION 'SMUM_XML_PARSE'
    EXPORTING
      xml_input = lv_xml_xstring
    TABLES
      xml_table = lt_xml_tab
      return    = lt_return.
  CHECK sy-subrc = 0.

  et_itab = lt_xml_tab.
  BREAK-POINT.


  "Use method like this:
  "zcl_conv_utilities=>xml_to_itab( EXPORTING iv_xml_from_applserver = '/usr/sap/trans/xml.xml' IMPORTING et_itab = DATA(lt_itab) ).
  "zcl_conv_utilities=>xml_to_itab( EXPORTING iv_xml_as_upload = abap_true IMPORTING et_itab = DATA(lt_itab) ).

ENDMETHOD.
ENDCLASS.
