class ZCL_MM_UTILITIES definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF ts_stock_rec,
        matnr         TYPE mard-matnr,
        mat_shorttext    TYPE makt-maktx,
        werks            TYPE mard-werks,
        lgort         TYPE mard-lgort,
        labst        TYPE mard-labst,
        insme        TYPE mard-insme,
        speme        TYPE mard-speme,
        partner_labst    TYPE mard-labst,
        partner_insme    TYPE mard-insme,
        partner_speme    TYPE mard-speme,
        partner_bstar    TYPE wvmi_stckt,
        partner_quantity TYPE mard-labst,
        partner_werks TYPE char10,
        partner_lgort TYPE char10,
        delta_labst      TYPE mard-labst,
        delta_insme      TYPE mard-insme,
        delta_speme      TYPE mard-speme,
        info             TYPE NATXT,
      END OF ts_stock_rec .
  types:
    tt_stock_rec TYPE TABLE OF ts_stock_rec .
  types:
    BEGIN OF ts_partner_sap_mapping,
        partner_bstar     TYPE char05, "wvmi_stckt,
        sap_bstar_understanding TYPE char05,
        partner_werks TYPE char10,
        sap_werks_understanding TYPE werks_d,
        partner_lgort TYPE char10,
        sap_lgort_understanding TYPE lgort_d,
      END OF ts_partner_sap_mapping .
  types:
    tt_partner_sap_mapping TYPE TABLE OF ts_partner_sap_mapping .

  class-data GS_MESSAGE type BAL_S_MSG .
  class-data GT_LOG_FOR_BAL_STATIC type BAL_T_MSG .
  constants GC_MC type MSGID value 'ZMC_MM_UTILITIES' ##NO_TEXT.

  class-methods CLASS_CONSTRUCTOR .
  class-methods GET_PO_CHANGE_DOCUMENTS
    importing
      !IS_EKKO type EKKO
      !IV_ADDRESS_NUMBER type EKKO-ADRNR optional
      !IV_DATE_OF_CHANGE type CDHDR-UDATE optional
      !IV_TIME_OF_CHANGE type CDHDR-UTIME optional
      !IV_PRINT_OPERATION type T166K-DRUVO
    changing
      !XT_CDHDR type /ISDFPS/CDHDR_TAB
      !XT_CDSHW type CDSHW_TAB
      !XT_EKPO type EKPO_TTY
      !XT_AEND type MEEIN_XAEND_ITAB .
  class-methods CREATE_STOCK_RECONCILIATION
    importing
      !IT_SAP_STOCKS type TT_STOCK_REC
      !IT_PARTNER_STOCKS type TT_STOCK_REC
      !IT_PARTNER_SAP_MAPPING type TT_PARTNER_SAP_MAPPING optional
      !IV_TEST_OUTPUT type WRF_TEST_FLAG optional
    exporting
      value(ET_DELTA) type TT_STOCK_REC .
  class-methods GET_QUAN_FROM_TARGET_UOM
    importing
      !IV_MATNR type MATNR
      !IV_SOURCE_QUAN_UOM type MEINS
      !IV_TARGET_QUAN_UOM type MEINS
      !IV_SOURCE_QUAN type BSTMG
    returning
      value(RV_TARGET_QUAN) type BSTMG .
  PROTECTED SECTION.
private section.

*  methods _ADD_MESSAGE
*    importing
*      !IV_SY type SYST .
  methods README .
ENDCLASS.



CLASS ZCL_MM_UTILITIES IMPLEMENTATION.


 method CLASS_CONSTRUCTOR.
  CLEAR: zcl_mm_utilities=>gt_log_for_bal_static, zcl_mm_utilities=>gs_message.
 endmethod.


  METHOD create_stock_reconciliation.

    DATA: lt_partner_stocks      TYPE TABLE OF zcl_mm_utilities=>ts_stock_rec,
          lt_prepare_part_stocks TYPE TABLE OF zcl_mm_utilities=>ts_stock_rec,
          ls_prepare_part_stocks LIKE LINE OF lt_prepare_part_stocks,
          ls_delta               LIKE LINE OF lt_prepare_part_stocks.

*  SAP / Partner Mapping

    "In the case that the partner stock quantity is determined using an indicator
    IF it_partner_sap_mapping IS NOT INITIAL.
      LOOP AT it_partner_stocks ASSIGNING FIELD-SYMBOL(<ls_partner_stocks>).

        READ TABLE it_partner_sap_mapping INTO DATA(ls_mapping) WITH KEY
                                                  partner_bstar = <ls_partner_stocks>-partner_bstar.
        IF sy-subrc = 0.
          CASE ls_mapping-sap_bstar_understanding.
            WHEN 'LABST'.
              ls_prepare_part_stocks-partner_labst = <ls_partner_stocks>-partner_quantity.
            WHEN 'INSME'.
              ls_prepare_part_stocks-partner_insme = <ls_partner_stocks>-partner_quantity.
            WHEN 'SPEME'.
              ls_prepare_part_stocks-partner_speme = <ls_partner_stocks>-partner_quantity.
            WHEN OTHERS.
              MESSAGE e005(zmc_mm_utilities) WITH 'PARTNER_BSTAR|SAP_BSTAR_UNDERSTANDING' INTO DATA(lv_msg).
              "zcl_bc_application_log=>add_sy_msg_to_bal_msg_tab( CHANGING xt_bal_msg = zcl_mm_utilities=>gt_log_for_bal_static ).
              MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
              ls_prepare_part_stocks-info = gs_message-msgty && gs_message-msgno.
          ENDCASE.
        ENDIF.

*  In the case that the partner werks is determined using an indicator
        CLEAR ls_mapping. READ TABLE it_partner_sap_mapping INTO ls_mapping WITH KEY
                                                partner_werks = <ls_partner_stocks>-partner_werks.

        "mapping values are correct
        IF sy-subrc = 0 AND ls_mapping-sap_werks_understanding IS NOT INITIAL.
          ls_prepare_part_stocks-partner_werks = ls_mapping-sap_werks_understanding.

          "no mapping for werks and partner delivers sap werks value
        ELSEIF ls_mapping-sap_werks_understanding IS INITIAL
                   AND ls_mapping-partner_werks IS INITIAL AND <ls_partner_stocks>-werks IS NOT INITIAL.
          ls_prepare_part_stocks-werks = <ls_partner_stocks>-werks.

          "mapping fields for werks are empty and partner does not delivers sap werks value => wrong output
        ELSEIF ls_mapping-sap_werks_understanding IS INITIAL
                   AND ls_mapping-partner_werks IS INITIAL AND <ls_partner_stocks>-werks IS INITIAL.
          CLEAR lv_msg. MESSAGE e006(zmc_mm_utilities) WITH 'PARTNER_WERKS|SAP_WERKS_UNDERSTANDING' 'WERKS' INTO lv_msg.
          "zcl_bc_application_log=>add_sy_msg_to_bal_msg_tab( CHANGING xt_bal_msg = zcl_mm_utilities=>gt_log_for_bal_static ).
          CLEAR zcl_mm_utilities=>gs_message. MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
          ls_prepare_part_stocks-info = ls_prepare_part_stocks-info && gs_message-msgty && gs_message-msgno.

          "all other cases
        ELSE.
          CLEAR lv_msg. MESSAGE e005(zmc_mm_utilities) WITH 'PARTNER_WERKS|SAP_WERKS_UNDERSTANDING' INTO lv_msg.
          "zcl_bc_application_log=>add_sy_msg_to_bal_msg_tab( CHANGING xt_bal_msg = zcl_mm_utilities=>gt_log_for_bal_static ).
          CLEAR zcl_mm_utilities=>gs_message. MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
          ls_prepare_part_stocks-info = ls_prepare_part_stocks-info && gs_message-msgty && gs_message-msgno.
        ENDIF.

*   In the case that the partner lgort is determined using an indicator
        CLEAR ls_mapping. READ TABLE it_partner_sap_mapping INTO ls_mapping WITH KEY
                                                partner_lgort = <ls_partner_stocks>-partner_lgort.
        "mapping values are correct
        IF sy-subrc = 0 AND ls_mapping-sap_lgort_understanding IS NOT INITIAL.
          ls_prepare_part_stocks-partner_lgort = ls_mapping-sap_lgort_understanding.

          "no mapping for lgort and partner delivers sap lgort value
        ELSEIF ls_mapping-sap_lgort_understanding IS INITIAL AND ls_mapping-partner_lgort IS INITIAL.
          ls_prepare_part_stocks-lgort = <ls_partner_stocks>-lgort.

          "mapping fields for werks are empty and partner does not delivers sap werks value => wrong output
        ELSEIF ls_mapping-sap_lgort_understanding IS INITIAL
                   AND ls_mapping-partner_lgort IS INITIAL AND <ls_partner_stocks>-lgort IS INITIAL.
          CLEAR lv_msg. MESSAGE e006(zmc_mm_utilities) WITH 'PARTNER_WERKS|SAP_WERKS_UNDERSTANDING' 'LGORT' INTO lv_msg.
          "zcl_bc_application_log=>add_sy_msg_to_bal_msg_tab( CHANGING xt_bal_msg = zcl_mm_utilities=>gt_log_for_bal_static ).
          CLEAR zcl_mm_utilities=>gs_message. MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
          ls_prepare_part_stocks-info = ls_prepare_part_stocks-info && gs_message-msgty && gs_message-msgno.

          "all other cases
        ELSE.
          CLEAR lv_msg. MESSAGE e005(zmc_mm_utilities) WITH 'PARTNER_LGORT|SAP_LGORT_UNDERSTANDING' INTO lv_msg.
          "zcl_bc_application_log=>add_sy_msg_to_bal_msg_tab( CHANGING xt_bal_msg = zcl_mm_utilities=>gt_log_for_bal_static ).
          CLEAR zcl_mm_utilities=>gs_message. MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
          ls_prepare_part_stocks-info = ls_prepare_part_stocks-info && gs_message-msgty && gs_message-msgno.
        ENDIF.

        ls_prepare_part_stocks-matnr = <ls_partner_stocks>-matnr.

        "Check if material is already in prepare partner itab
        READ TABLE lt_prepare_part_stocks INTO DATA(ls_mat_already_in) WITH KEY matnr = <ls_partner_stocks>-matnr.
        IF sy-subrc = 0. "Already a entry with this matnr
          COLLECT ls_prepare_part_stocks INTO lt_prepare_part_stocks.
        ELSE.
          APPEND ls_prepare_part_stocks TO lt_prepare_part_stocks.
        ENDIF.
        CLEAR ls_prepare_part_stocks.

      ENDLOOP.
      lt_partner_stocks = lt_prepare_part_stocks.
    ELSE.
      lt_partner_stocks = it_partner_stocks.
    ENDIF.


    "Now do the diffs that means the delta
    LOOP AT lt_partner_stocks ASSIGNING <ls_partner_stocks>.
      READ TABLE it_sap_stocks INTO DATA(ls_sap_stocks) WITH KEY matnr = <ls_partner_stocks>-matnr
                                                                 werks = <ls_partner_stocks>-werks
                                                                 lgort = <ls_partner_stocks>-lgort.
      IF sy-subrc = 4.
        "Try again with internal type number of material
        <ls_partner_stocks>-matnr = |{ <ls_partner_stocks>-matnr ALPHA = IN }|.
        CLEAR ls_sap_stocks.
        READ TABLE it_sap_stocks INTO ls_sap_stocks WITH KEY matnr = <ls_partner_stocks>-matnr
                                                                   werks = <ls_partner_stocks>-werks
                                                                   lgort = <ls_partner_stocks>-lgort.
      ENDIF.

      IF sy-subrc = 0.
        ls_delta-labst = ls_sap_stocks-labst.
        ls_delta-insme = ls_sap_stocks-insme.
        ls_delta-speme = ls_sap_stocks-speme.

        ls_delta-partner_labst = <ls_partner_stocks>-partner_labst.
        ls_delta-partner_insme = <ls_partner_stocks>-partner_insme.
        ls_delta-partner_speme = <ls_partner_stocks>-partner_speme.

        ls_delta-delta_labst = ls_delta-labst - ls_delta-partner_labst.
        ls_delta-delta_insme = ls_delta-insme - ls_delta-partner_insme.
        ls_delta-delta_speme = ls_delta-speme - ls_delta-partner_speme.

        ls_delta-info = <ls_partner_stocks>-info.

      ELSE.
        MESSAGE e004(zmc_mm_utilities) INTO DATA(lv_no_stoc_loc_data).
        CLEAR zcl_mm_utilities=>gs_message.MOVE-CORRESPONDING sy TO zcl_mm_utilities=>gs_message.
        ls_delta-info = <ls_partner_stocks>-info && gs_message-msgty && gs_message-msgno.
      ENDIF.

      "Fill this data always, because general data for showing in output and better error handling
      ls_delta-matnr = <ls_partner_stocks>-matnr.
      SELECT SINGLE maktx FROM makt WHERE matnr = @ls_sap_stocks-matnr AND spras = @sy-langu INTO @ls_delta-mat_shorttext.
      ls_delta-werks = <ls_partner_stocks>-werks.
      ls_delta-lgort = <ls_partner_stocks>-lgort.

      "Put at last MC in info for all msgs
      IF ls_delta-info IS NOT INITIAL.ls_delta-info = ls_delta-info && '_' && zcl_mm_utilities=>gc_mc.ENDIF.

      APPEND ls_delta TO et_delta.
      CLEAR ls_delta.
    ENDLOOP.

    CHECK iv_test_output = abap_true.
    cl_demo_output=>display_data( EXPORTING name = 'TEST' value = et_delta ).

  ENDMETHOD.                                             "#EC CI_VALPAR


    METHOD get_po_change_documents.

      IF is_ekko-ebeln IS INITIAL.
        RAISE EXCEPTION TYPE zcx_mm_utilities MESSAGE e003(zmc_mm_utilities).
      ENDIF.

      CALL FUNCTION 'ME_READ_CHANGES_EINKBELEG'
        EXPORTING
          address_number  = iv_address_number
          document        = is_ekko
          date_of_change  = iv_date_of_change
          time_of_change  = iv_time_of_change
          print_operation = iv_print_operation
        TABLES
          xcdhdr          = xt_cdhdr
          xcdshw          = xt_cdshw
          xekpo           = xt_ekpo
          xaend           = xt_aend.

    ENDMETHOD.


    METHOD get_quan_from_target_uom.

      DATA lv_matnr TYPE matnr.

      "Convert to input format
      IF strlen( iv_matnr ) < 18.
        lv_matnr = |{ iv_matnr ALPHA = IN }|.
      ELSE.
        lv_matnr = iv_matnr.
      ENDIF.

      CALL FUNCTION 'MD_CONVERT_MATERIAL_UNIT'
        EXPORTING
          i_matnr              = lv_matnr
          i_in_me              = iv_source_quan_uom
          i_out_me             = iv_target_quan_uom
          i_menge              = iv_source_quan
        IMPORTING
          e_menge              = rv_target_quan
        EXCEPTIONS
          error_in_application = 1
          error                = 2
          OTHERS               = 3.

      IF sy-subrc <> 0.
        CASE sy-subrc.
          WHEN 1.
            RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e002(zmc_mm_utilities).
          WHEN 2 OR 3.
            RAISE EXCEPTION TYPE zcx_conv_utilities MESSAGE e001(zmc_mm_utilities).
        ENDCASE.
      ENDIF.

    ENDMETHOD.


METHOD readme.
** CLASS CONSTRUCTOR

"clears global variables for new use in different static methods.

* CREATE_STOCK_RECONCILIATION:

"This method creates an stock reconciliation. It needs tables for sap stocks, partner, stocks and optional its possible to do a mapping.
"The mapping means e.g. the partner delivers the stocks via an stock indicator and a single value for the quantity.
"In this case the partner stock contains values in the fields "partner_bsar" and "partner_quantity" (furhter possible with werks & lgort).
"So the partner stocks fields "partner_labst", "partner_insme" etc. are empty.
"When you are doing the mapping, you have to build up the mapping for stock indicator: "partner_bstar" and "sap_bstar_understanding"
"e.g.: partner_bstar = 'FREE' and "sap_bstar_understanding = 'LABST'. The values LABST;INSME AND SPEME are fixed.

*Example code

*DATA(lt_mard) = zcl_mm_utilities=>get_storage_loc_data( ir_materials = lr_matnr
*                                                        ir_plants = lr_werks
*                                                        ir_stor_locs = lr_lgort ).
*
*DATA: lt_sap_stocks TYPE zcl_mm_utilities=>tt_stock_rec,
*      ls_partner_stocks TYPE zcl_mm_utilities=>ts_stock_rec,
*      lt_partner_stocks TYPE zcl_mm_utilities=>tt_stock_rec,
*      ls_mapping TYPE zcl_mm_utilities=>ts_partner_sap_mapping,
*      lt_mapping TYPE zcl_mm_utilities=>tt_partner_sap_mapping.
*
*MOVE-CORRESPONDING lt_mard TO lt_sap_stocks.
*
*"In case of bstar
*ls_partner_stocks-matnr = 'EWMS4-42'.
*ls_partner_stocks-werks = '1710'.
*ls_partner_stocks-lgort = '171S'.
*ls_partner_stocks-partner_bstar = 'FREE'. "partner delivers only stock identifier
*ls_mapping-partner_bstar = ls_partner_stocks-partner_bstar. "add bstar to mapping and the equivalent to SAP
*ls_mapping-sap_bstar_understanding = 'LABST'.
*ls_partner_stocks-partner_quantity = '1200'. "partners delivers single quantity field
*APPEND ls_partner_stocks TO lt_partner_stocks.
*APPEND ls_mapping TO lt_mapping.
*CLEAR: ls_partner_stocks, ls_partner_stock_intpret.

"mapping werks
*ls_partner_stocks-matnr = 'EWMS4-42'.
*"Field ls_partner_stocks-werks is empty. Field for SAP werks
*ls_partner_stocks-lgort = '171S'.
*ls_partner_stocks-partner_insme = '1200'. "partner delivers value for quality stock directly
*ls_partner_stocks-partner_werks = '15'. "partner_werks means the value that the partners save the plant
*ls_mapping-partner_werks = '15'. "partner save the plant different
*ls_mapping-sap_werks_understanding = '1710'. "sap plant value
*APPEND ls_mapping TO lt_mapping.
*APPEND ls_partner_stocks TO lt_partner_stocks.
*CLEAR: ls_partner_stocks, ls_mapping.
*
*zcl_mm_utilities=>create_stock_reconciliation( EXPORTING it_partner_sap_mapping = lt_mapping
*                                                                it_partner_stocks = lt_partner_stocks
*                                                                it_sap_stocks = lt_sap_stocks
*                                                                iv_test_output = abap_true
*                                                                IMPORTING et_delta = DATA(lt_delta) ).



 "CREATED by MARVIN MICHEL in his freetime ;)
 BREAK gitlab/m1ch3l-de/abap-code-collection ##NO_BREAK .
ENDMETHOD.
ENDCLASS.
