Hi,

Here you can find my Z-Classes and Z-Programs, which I programmed in my spare time. 
This must be mentioned explicitly, so that the impression does not arise 
that this code was taken from customer projects from my employment as an IT consultant.

So the code is free to use.

Maybe you can find something for you ;)

Best Marvin
