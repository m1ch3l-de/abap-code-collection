class ZCL_MONITORING_UTILITIES definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF TS_SLG1_HEAD_MSG_DATA,
    mandant TYPE MANDT,
    lognumber TYPE BALOGNR,
    object TYPE BALOBJ_D,
    subobject TYPE BALSUBOBJ,
    extnumber TYPE BALNREXT,
    aldate TYPE BALDATE,
    altime TYPE BALTIME,
    altcode TYPE BALTCODE,
    alprog TYPE BALPROG,
    protocoltext(273) TYPE c,
   END OF TS_SLG1_HEAD_MSG_DATA .
  types:
    TT_SLG1_HEAD_MSG_DATA TYPE TABLE OF TS_SLG1_HEAD_MSG_DATA .
  types:
    tt_range_idoc_status     TYPE RANGE OF edids-status .
  types:
    tt_range_idoc_statustype TYPE RANGE OF edi_symsty .
  types:
    BEGIN OF TS_CHAR_TABLE,
    line(999) TYPE c,
   END OF TS_CHAR_TABLE .
  types:
    TT_CHAR_TABLE TYPE TABLE OF TS_CHAR_TABLE .

  constants:
    BEGIN OF gcs_mode_is,
        dump TYPE i VALUE 0,
        slg1 TYPE i VALUE 1,
        idoc TYPE i VALUE 2,
      END OF gcs_mode_is .

  methods CONSTRUCTOR
    importing
      !IV_SET_MONITORING type I
      !IV_DUMPS_DATE type D optional
      !IV_SLG1_OBJECT type BALHDR-OBJECT optional
      !IV_SLG1_SUBOBJECT type BALHDR-SUBOBJECT optional
      !IV_SLG1_DATE_FROM type BALHDR-ALDATE default SY-DATUM
      !IV_SLG1_DATE_TO type BALHDR-ALDATE default SY-DATUM
      !IV_SLG1_TIME_FROM type BALHDR-ALTIME optional
      !IV_SLG1_TIME_TO type BALHDR-ALTIME default SY-UZEIT
      !IV_SLG1_PROGRAMNAME type BALHDR-ALPROG optional
      !IV_SLG1_TCODE type BALHDR-ALTCODE optional
      !IV_IDOC_LOGDATE_GE type EDI_LOGDAT optional
      !IV_IDOC_LOGTIME_GE type EDI_LOGTIM optional
      !IT_IDOC_STATUS_RANGE type TT_RANGE_IDOC_STATUS optional
      !IT_IDOC_STTYPE_RANGE type TT_RANGE_IDOC_STATUSTYPE optional
    raising
      ZCX_MONITORING_UTILITIES .
  methods SHOW_ON_GUI .
  methods SEND_VIA_EMAIL
    importing
      !IV_DISTRIBUTIONLIST type SO_OBJ_NAM
    raising
      ZCX_MONITORING_UTILITIES
      CX_SEND_REQ_BCS
      CX_DOCUMENT_BCS
      CX_ADDRESS_BCS .
  methods RETURN_AS_ITAB
    returning
      value(RT_ITAB) type ref to DATA .
  methods SAVE_ON_APPLSERVER
    importing
      !IV_PATH_NAME_TO_APPLSERVER type STRING
      !IV_DELIMETER type TRENNZ optional
    raising
      ZCX_MONITORING_UTILITIES .
  methods GET_MESSAGES
    returning
      value(RT_MESSAGES) type BAL_T_MSG .
  methods GET_CURRENT_MODE
    returning
      value(RT_CURRENT_MODE) type I .
  methods LOG_MSGS_TO_BAL .
protected section.

  data _GV_BAL_HANDLE type BALLOGHNDL .
private section.

  data _GV_CURRENT_MODE type I .
  data _GV_CURRENT_MODE_TEXT type STRING .
  data _GV_CURRENT_MODE_DATE type SY-DATUM .
  data _GR_CURRENT_REF type ref to DATA .
  data _GT_DUMPS type RSDUMPTAB .
  data _GT_IDOC_STATUS_ENT type EDIDS_TT .
  data _GT_SLG1_HEADER_ENT type BALHDR_T .
  data _GT_SLG1_MSG_ENT type BALM_T .
  data _GT_SLG1_HEAD_MSG_ENT type TT_SLG1_HEAD_MSG_DATA .
  data _GT_MESSAGES type BAL_T_MSG .
  class-data _GS_MESSAGE type BAL_S_MSG .
  data _GT_MESSAGES_ERROR_OUTPUT type TT_CHAR_TABLE .
  data _GO_BAL type ref to IF_RECA_MESSAGE_LIST .
  data _GC_MC type ARBGB value 'zmc_monti_utilities' ##NO_TEXT.

  methods _SET_MSG_OUTPUT_IN_MODE .
  methods _SET_MODE_DETAILS
    importing
      !IV_CURRENT_MODE type I optional
      !IV_CURRENT_MODE_TEXT type STRING
      !IV_CURRENT_MODE_DATE type SY-DATUM .
  methods _GET_DUMPS
    importing
      !IV_DUMPS_DATE type D
    raising
      ZCX_MONITORING_UTILITIES .
  methods _GET_SLG1_ENTRIES
    importing
      !IV_SLG1_OBJECT type BALHDR-OBJECT optional
      !IV_SLG1_SUBOBJECT type BALHDR-SUBOBJECT optional
      !IV_SLG1_DATE_FROM type BALHDR-ALDATE optional
      !IV_SLG1_DATE_TO type BALHDR-ALDATE optional
      !IV_SLG1_TIME_FROM type BALHDR-ALTIME optional
      !IV_SLG1_TIME_TO type BALHDR-ALTIME optional
      !IV_SLG1_PROGRAMNAME type BALHDR-ALPROG optional
      !IV_SLG1_TCODE type BALHDR-ALTCODE optional
    raising
      ZCX_MONITORING_UTILITIES .
  methods _GET_IDOC_STATUSSETS
    importing
      !IV_IDOC_LOGDATE_GE type EDI_LOGDAT optional
      !IV_IDOC_LOGTIME_GE type EDI_LOGTIM optional
      !IT_IDOC_STATUS_RANGE type TT_RANGE_IDOC_STATUS optional
      !IT_IDOC_STTYPE_RANGE type TT_RANGE_IDOC_STATUSTYPE optional
    raising
      ZCX_MONITORING_UTILITIES .
  methods _ADD_MESSAGE
    importing
      !IV_SY type SYST optional .
  methods README .
ENDCLASS.



CLASS ZCL_MONITORING_UTILITIES IMPLEMENTATION.


  METHOD constructor.

    TRY.

        CASE iv_set_monitoring.

          WHEN me->gcs_mode_is-dump.

            me->_set_mode_details( iv_current_mode = me->gcs_mode_is-dump
                                   iv_current_mode_text = 'DUMPS'
                                   iv_current_mode_date = iv_dumps_date ).

            me->_get_dumps( iv_dumps_date = iv_dumps_date ).

          WHEN me->gcs_mode_is-slg1.

            me->_set_mode_details( iv_current_mode = me->gcs_mode_is-slg1
                                   iv_current_mode_text = 'BAL'
                                   iv_current_mode_date = iv_slg1_date_from ).

            me->_get_slg1_entries( iv_slg1_object = iv_slg1_object
                                   iv_slg1_subobject = iv_slg1_subobject
                                   iv_slg1_date_from = iv_slg1_date_from
                                   iv_slg1_date_to = iv_slg1_date_to
                                   iv_slg1_time_from = iv_slg1_time_from
                                   iv_slg1_time_to = iv_slg1_time_to
                                   iv_slg1_programname = iv_slg1_programname
                                   iv_slg1_tcode = iv_slg1_tcode ).

          WHEN me->gcs_mode_is-idoc.

            me->_set_mode_details(    iv_current_mode = me->gcs_mode_is-idoc
                                      iv_current_mode_text = 'IDOC'
                                      iv_current_mode_date = iv_idoc_logdate_ge ).

            me->_get_idoc_statussets( iv_idoc_logdate_ge = iv_idoc_logdate_ge
                                      iv_idoc_logtime_ge = iv_idoc_logtime_ge
                                      it_idoc_status_range = it_idoc_status_range
                                      it_idoc_sttype_range = it_idoc_sttype_range ).

          WHEN OTHERS.

            MESSAGE e004(zmc_monti_utilities) INTO DATA(lv_unknown_mode).
            me->_add_message( iv_sy = sy ).
            RAISE EXCEPTION TYPE zcx_monitoring_utilities.
        ENDCASE.

      CATCH zcx_monitoring_utilities INTO DATA(exp_mon).
        me->_set_msg_output_in_mode( ). "instead of returning monitoring protocols, return error msgs
    ENDTRY.

  ENDMETHOD.


  METHOD return_as_itab.
    rt_itab = me->_gr_current_ref.
  ENDMETHOD.


  METHOD save_on_applserver.

    DATA: lt_data_comma_sep    TYPE rsanm_file_table,
          ls_data_comma_sep    TYPE rsanm_file_line,
          lv_comp_counter      TYPE i,
          lv_assign_comp_subrc TYPE sy-subrc,
          lv_last_comma        TYPE i,
          lv_delimeter         TYPE trennz.
    FIELD-SYMBOLS: <lt_protocols>     TYPE ANY TABLE,
                   <lt_protocols_tmp> TYPE ANY TABLE.

    TRY.

        ASSIGN me->_gr_current_ref->* TO <lt_protocols>.
        ASSIGN me->_gr_current_ref->* TO <lt_protocols_tmp>.

        IF iv_delimeter IS INITIAL.
          lv_delimeter = ','.
        ELSE.
          lv_delimeter = iv_delimeter.
        ENDIF.

        LOOP AT <lt_protocols> ASSIGNING FIELD-SYMBOL(<ls_protocol>).

          WHILE lv_assign_comp_subrc = 0.
            lv_comp_counter = lv_comp_counter + 1.
            ASSIGN COMPONENT lv_comp_counter OF STRUCTURE <ls_protocol> TO FIELD-SYMBOL(<ls_comp>).
            IF sy-subrc = 0.
              ls_data_comma_sep = ls_data_comma_sep && <ls_comp> && lv_delimeter.
            ELSE.
              "needed because of replace sy-subrc will be to 0
              lv_assign_comp_subrc = 4.
              "delete last delimter
              lv_last_comma = strlen( ls_data_comma_sep ) - 1. "e.g. length is 131, last char is 130
              REPLACE SECTION OFFSET lv_last_comma LENGTH 1 OF ls_data_comma_sep WITH space.
            ENDIF.
          ENDWHILE.

          APPEND ls_data_comma_sep TO lt_data_comma_sep.
          CLEAR: ls_data_comma_sep, lv_comp_counter, lv_assign_comp_subrc.

        ENDLOOP.

        cl_rsan_ut_appserv_file_writer=>appserver_file_write( EXPORTING i_filename      = iv_path_name_to_applserver "max 255 char in AL11
                                                                        i_data_tab      = lt_data_comma_sep
                                                                        i_overwrite     = abap_true
                                                              IMPORTING e_lines_written = DATA(lt_lines_written)
                                                              EXCEPTIONS open_failed  = 1
                                                                         write_failed = 2
                                                                         close_failed = 3  ).

        IF sy-subrc <> 0.
          RAISE EXCEPTION TYPE zcx_monitoring_utilities
            MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
        ENDIF.

       MESSAGE s075(zmc_monti_utilities) INTO DATA(lv_file_created) WITH iv_path_name_to_applserver.
       me->_add_message( EXPORTING iv_sy = sy ).
       MESSAGE s075(zmc_monti_utilities) WITH iv_path_name_to_applserver.

      CATCH zcx_monitoring_utilities INTO DATA(zcx_mon).
       MESSAGE e076(zmc_monti_utilities) INTO DATA(lv_file_creating_error) WITH iv_path_name_to_applserver.
       me->_add_message( EXPORTING iv_sy = sy ).
       MESSAGE s076(zmc_monti_utilities) DISPLAY LIKE 'E' WITH iv_path_name_to_applserver.
    ENDTRY.
  ENDMETHOD.


  METHOD send_via_email.
    DATA: lo_send_request  TYPE REF TO cl_bcs,
          lo_document      TYPE REF TO cl_document_bcs,
          lo_sender        TYPE REF TO cl_sapuser_bcs,
          lo_receiver      TYPE REF TO if_recipient_bcs,
          lt_mail_text_tab TYPE soli_tab,
          lv_sub           TYPE string,
          lv_subject       TYPE so_obj_des.

    FIELD-SYMBOLS: <lt_current_ref_protocols> TYPE ANY TABLE.

    TRY.

        lo_send_request = cl_bcs=>create_persistent( ).

        "Buid subject
        IF me->_gv_current_mode_date = sy-datum.
          MESSAGE s054(zmc_monti_utilities) WITH me->_gv_current_mode_text me->_gv_current_mode_date sy-sysid INTO lv_sub.
        ELSE.
          MESSAGE s055(zmc_monti_utilities) WITH me->_gv_current_mode_text me->_gv_current_mode_date sy-sysid INTO lv_sub.
        ENDIF.
        "Subject with right type for method
        lv_subject = lv_sub.

        "E-Mail Text
        MESSAGE s050(zmc_monti_utilities) INTO DATA(lv_salut).
        APPEND lv_salut TO lt_mail_text_tab. APPEND ' ' TO lt_mail_text_tab.
        MESSAGE s051(zmc_monti_utilities) INTO DATA(lv_hereyouget) WITH me->_gv_current_mode_text me->_gv_current_mode_date.
        APPEND lv_hereyouget TO lt_mail_text_tab.
        APPEND ' ' TO lt_mail_text_tab.
        ASSIGN me->_gr_current_ref->* TO <lt_current_ref_protocols>.
        LOOP AT <lt_current_ref_protocols> ASSIGNING FIELD-SYMBOL(<ls_protocol>).
          DESCRIBE FIELD <ls_protocol> TYPE DATA(lv_type) LENGTH DATA(lv_structure_len) IN CHARACTER MODE.
          IF lv_structure_len <= 255.
            APPEND <ls_protocol> TO lt_mail_text_tab.
          ELSE.
            APPEND <ls_protocol>(255) TO lt_mail_text_tab.
          ENDIF.
          APPEND ' ' TO lt_mail_text_tab.
        ENDLOOP.
        MESSAGE s052(zmc_monti_utilities) INTO DATA(lv_regards).
        APPEND lv_regards TO lt_mail_text_tab.
        MESSAGE s053(zmc_monti_utilities) WITH sy-sysid sy-datum sy-sysid INTO DATA(lv_your_system).
        APPEND lv_your_system TO lt_mail_text_tab.

        lo_document = cl_document_bcs=>create_document( i_type    = 'RAW'
                                                        i_text    = lt_mail_text_tab
                                                        i_subject = lv_subject ).

        lo_send_request->set_document( lo_document ).

        lo_sender = cl_sapuser_bcs=>create( sy-uname ).
        lo_send_request->set_sender( lo_sender ).

        lo_receiver = cl_distributionlist_bcs=>getu_persistent( i_dliname = iv_distributionlist i_private = abap_false ).
        lo_send_request->add_recipient( lo_receiver ).

        lo_send_request->send( ).

        COMMIT WORK.

        MESSAGE s057(zmc_monti_utilities) INTO DATA(lv_email_created).
        me->_add_message( EXPORTING iv_sy = sy ).
        MESSAGE s057(zmc_monti_utilities).


      CATCH zcx_monitoring_utilities INTO DATA(exp_mon).
      CATCH cx_send_req_bcs.
      CATCH cx_document_bcs.
      CATCH cx_address_bcs.
       MESSAGE e056(zmc_monti_utilities) INTO DATA(lv_no_dist_list).
       me->_add_message( EXPORTING iv_sy = sy ).
       MESSAGE s056(zmc_monti_utilities) DISPLAY LIKE 'E'.

    ENDTRY.

  ENDMETHOD.


  METHOD show_on_gui.

    FIELD-SYMBOLS <lt_output> TYPE any table.

    ASSIGN me->_gr_current_ref->* TO <lt_output>.
    cl_demo_output=>display_data( EXPORTING name = me->_gv_current_mode_text && 'OUTPUT' value = <lt_output> ).
  ENDMETHOD.


method README.
*============== CONSTRUCTOR ==============*
"j
*1.DUMPS

 "Example of use:
 "DATA(go_dumps) = NEW zcl_monitoring_utilities( iv_set_monitoring = zcl_monitoring_utilities=>gcs_mode_is-dump
 "                                               iv_dumps_date = sy-datum ).

*2.SLG1
"Example of use:
"DATA(go_slg1) = NEW zcl_monitoring_utilities( iv_set_monitoring = zcl_monitoring_utilities=>gcs_mode_is-slg1
"                                              iv_slg1_object = '/UIF/LREP'
"                                              iv_slg1_date_from = '20210113'
"                                              iv_slg1_date_to = '20210113'
"                                              iv_slg1_time_from = '130000'
"                                              iv_slg1_time_to = '150000' ).

*3.IDOC

 "Use for Parameter IV_STATUS_RANGE AND IV_STATUSTYPE_RANGE ranges or directly SELECT-OPTIONS:
 "SELECT-OPTIONS so_sta FOR edids-status.
 "types: tt_range_idoc_status TYPE RANGE OF edids-status .
 "DATA: it_range_status TYPE tt_range_idoc_status.
 "APPEND LINES OF so_sta TO it_range_status.

 "Example of use:
 "DATA lv_date TYPE EDI_LOGDAT .
 "lv_date = sy-datum - 7.
 "DATA(go_idoc) = NEW zcl_monitoring_utilities( iv_set_monitoring = zcl_monitoring_utilities=>gcs_mode_is-idoc
 "                                              iv_idoc_logdate_ge = lv_date
 "                                              it_idoc_status_range = it_range_status ).
*============== SHOW_ON_GUI ==============*
"Example of use:
"go_dump/slg1/idoc->show_on_gui( ).
*============== SEND_VIA_EMAIL ==============*
"Example of use:
"go_dump/slg1/idoc->show_on_gui( EXPORTING iv_distributionlist = 'TEST2' ).
*============== SAVE_ON_APPLSERVER ==============*
"Example of use:
"go_dump/slg1/idoc->save_on_applserver( EXPORTING iv_path_name_to_applserver = '/usr/sap/trans/test3.csv' ).
*============== SAVE_ON_APPLSERVER ==============*
"Example of use:
"DATA(itab) = go_idoc->return_as_itab( ).

* CLASS was CREATED by MARVIN MICHEL in his freetime ;)
BREAK gitlab/m1ch3l-de/abap-code-collection ##NO_BREAK.
endmethod.


method GET_MESSAGES.
 rt_messages = me->_gt_messages.
endmethod.


method _ADD_MESSAGE.
 CLEAR me->_gs_message.
 MOVE-CORRESPONDING iv_sy TO _gs_message.
 APPEND me->_gs_message TO me->_gt_messages.
endmethod.


METHOD _set_mode_details.
  me->_gv_current_mode = iv_current_mode.
  me->_gv_current_mode_text = iv_current_mode_text.
  me->_gv_current_mode_date = iv_current_mode_date.

ENDMETHOD.


METHOD _get_dumps.

  IF iv_dumps_date IS INITIAL.
    MESSAGE e002(zmc_monti_utilities) INTO DATA(lv_dump_date_initial) WITH me->_gv_current_mode_text 'IV_DUMPS_DATE' '' ''.
    me->_add_message( iv_sy = sy ).
    RAISE EXCEPTION TYPE zcx_monitoring_utilities.
  ENDIF.

  CALL FUNCTION 'RS_ST22_GET_DUMPS'
    EXPORTING
      p_day        = iv_dumps_date
    IMPORTING
      p_infotab    = me->_gt_dumps
    EXCEPTIONS
      no_authority = 1
      OTHERS       = 2.
  IF sy-subrc <> 0.
    CASE sy-subrc.
      WHEN 1.
        MESSAGE e000(zmc_monti_utilities) INTO DATA(lv_error_1) WITH 'FM: RS_ST22_GET_DUMPS' 'SUBRC' '1' 'NO_AUTHORITY'.
        me->_add_message( iv_sy = sy ).
        RAISE EXCEPTION TYPE zcx_monitoring_utilities.
      WHEN 2.
        MESSAGE e000(zmc_monti_utilities) INTO DATA(lv_error_2) WITH 'FM: RS_ST22_GET_DUMPS' 'SUBRC' '2' 'OTHERS'.
        me->_add_message( iv_sy = sy ).
        RAISE EXCEPTION TYPE zcx_monitoring_utilities.
      WHEN OTHERS.
        MESSAGE e000(zmc_monti_utilities) INTO DATA(lv_error_o) WITH 'FM: RS_ST22_GET_DUMPS' 'SUBRC' 'OTHER THAN 1 OR 2' 'UNKNOWN ERROR'.
        me->_add_message( iv_sy = sy ).
        RAISE EXCEPTION TYPE zcx_monitoring_utilities MESSAGE e000(zmc_monti_utilities) WITH 'FM: RS_ST22_GET_DUMPS' 'SUBRC' 'OTHER THAN 1 OR 2' 'UNKNOWN ERROR'.
    ENDCASE.
  ENDIF.

  IF sy-subrc = 0 AND me->_gt_dumps IS INITIAL.
    MESSAGE i003(zmc_monti_utilities) INTO DATA(lv_idoc_no_entries) WITH me->_gv_current_mode_text.
    me->_add_message( iv_sy = sy ).
    RAISE EXCEPTION TYPE zcx_monitoring_utilities.
  ENDIF.

  GET REFERENCE OF _gt_dumps INTO me->_gr_current_ref.
ENDMETHOD.


METHOD _get_idoc_statussets.
  SELECT * FROM edids INTO TABLE _gt_idoc_status_ent WHERE logdat GE iv_idoc_logdate_ge AND
                                                    logtim GE iv_idoc_logtime_ge AND
                                                    status IN it_idoc_status_range AND
                                                    statyp IN it_idoc_sttype_range.
  IF sy-subrc <> 0.
    MESSAGE i003(zmc_monti_utilities) INTO DATA(lv_idoc_no_entries) WITH me->_gv_current_mode_text.
    me->_add_message( iv_sy = sy ).
    RAISE EXCEPTION TYPE zcx_monitoring_utilities.
  ENDIF.

  GET REFERENCE OF _gt_idoc_status_ent INTO me->_gr_current_ref.
ENDMETHOD.


METHOD _get_slg1_entries.
  DATA: lt_header_data TYPE TABLE OF balhdr,
        lt_msg_data    TYPE TABLE OF balm.

  "APPL_LOG_READ_DB
  CALL FUNCTION 'APPL_LOG_READ_DB'
    EXPORTING
      object           = iv_slg1_object
      subobject        = iv_slg1_subobject
      date_from        = iv_slg1_date_from
      date_to          = iv_slg1_date_to
      time_from        = iv_slg1_time_from
      time_to          = iv_slg1_time_to
      log_class        = '4'
      program_name     = iv_slg1_programname
      transaction_code = iv_slg1_tcode
      mode             = '+'
    TABLES
      header_data      = lt_header_data
      messages         = lt_msg_data.
*          T_EXCEPTIONS      =

  IF sy-subrc <> 0.
    MESSAGE e000(zmc_monti_utilities) INTO DATA(lv_bal_logread_error) WITH 'APPL_LOG_READ_DB' 'SUBRC' '<> 0' 'ERROR'.
    me->_add_message( iv_sy = sy ).
    RAISE EXCEPTION TYPE zcx_monitoring_utilities.
  ELSEIF sy-subrc = 0 AND lt_header_data IS INITIAL AND lt_msg_data IS INITIAL.
    "MESSAGE i003(zmc_monti_utilities) INTO DATA(lv_idoc_no_entries) WITH me->_gv_current_mode_text.
    MESSAGE e003(zmc_monti_utilities) INTO DATA(lv_idoc_no_entries) WITH me->_gv_current_mode_text.
    me->_add_message( iv_sy = sy ).
    RAISE EXCEPTION TYPE zcx_monitoring_utilities.
  ENDIF.

  "Raw data
  me->_gt_slg1_header_ent = lt_header_data.
  me->_gt_slg1_msg_ent = lt_msg_data.

  "Create protocol from header and message table
  DATA: ls_head_msg TYPE me->ts_slg1_head_msg_data,
        lt_head_msg TYPE me->tt_slg1_head_msg_data,
        var_counter TYPE i.

  LOOP AT me->_gt_slg1_msg_ent ASSIGNING FIELD-SYMBOL(<ls_slg1_msg>).
    READ TABLE me->_gt_slg1_header_ent ASSIGNING FIELD-SYMBOL(<ls_slg1_head>)
                                                   WITH KEY lognumber = <ls_slg1_msg>-lognumber.
    IF sy-subrc = 0.
      MOVE-CORRESPONDING <ls_slg1_head> TO ls_head_msg.
    ENDIF.

    SELECT SINGLE * FROM t100 INTO @DATA(ls_t100_msg) WHERE arbgb = @<ls_slg1_msg>-msgid AND
                                                            sprsl = @sy-langu AND
                                                            msgnr = @<ls_slg1_msg>-msgno.
    IF sy-subrc = 0.
      ls_head_msg-protocoltext = ls_t100_msg-text.
      REPLACE FIRST OCCURRENCE OF '&1' IN ls_head_msg-protocoltext WITH <ls_slg1_msg>-msgv1.
      REPLACE FIRST OCCURRENCE OF '&2' IN ls_head_msg-protocoltext WITH <ls_slg1_msg>-msgv2.
      REPLACE FIRST OCCURRENCE OF '&3' IN ls_head_msg-protocoltext WITH <ls_slg1_msg>-msgv3.
      REPLACE FIRST OCCURRENCE OF '&4' IN ls_head_msg-protocoltext WITH <ls_slg1_msg>-msgv4.
    ENDIF.

    APPEND ls_head_msg TO lt_head_msg.
    CLEAR ls_head_msg.
  ENDLOOP.

  me->_gt_slg1_head_msg_ent = lt_head_msg. "lt_head_msg directly to _gr_current_ref throws an exp "not assign fieldsymbol in show_via_gui(), table must be global in class.
  GET REFERENCE OF me->_gt_slg1_head_msg_ent INTO me->_gr_current_ref.
ENDMETHOD.


METHOD _SET_MSG_OUTPUT_IN_MODE.
    DATA: lv_msg_txt TYPE string.

    MESSAGE i026(zmc_monti_utilities) INTO DATA(lv_processing_errors) WITH me->_gv_current_mode_text.
    CLEAR _gs_message. MOVE-CORRESPONDING sy TO _gs_message.
    INSERT _gs_message INTO me->_gt_messages INDEX 1.

    SELECT * FROM t100 INTO TABLE @DATA(lt_all_mont_msg) WHERE arbgb = @me->_gc_mc AND sprsl = @sy-langu.

    LOOP AT me->_gt_messages ASSIGNING FIELD-SYMBOL(<ls_msg>).

      READ TABLE lt_all_mont_msg INTO DATA(ls_msg_from_t100) WITH KEY msgnr = <ls_msg>-msgno.
      IF sy-subrc = 0.
        lv_msg_txt = ls_msg_from_t100-text.
        REPLACE FIRST OCCURRENCE OF '&1' IN lv_msg_txt WITH <ls_msg>-msgv1.
        REPLACE FIRST OCCURRENCE OF '&2' IN lv_msg_txt WITH <ls_msg>-msgv2.
        REPLACE FIRST OCCURRENCE OF '&3' IN lv_msg_txt WITH <ls_msg>-msgv3.
        REPLACE FIRST OCCURRENCE OF '&4' IN lv_msg_txt WITH <ls_msg>-msgv4.
      ENDIF.
      APPEND lv_msg_txt TO me->_gt_messages_error_output.
    ENDLOOP.

    GET REFERENCE OF me->_gt_messages_error_output INTO me->_gr_current_ref.
ENDMETHOD.


method GET_CURRENT_MODE.
 rt_current_mode = me->_gv_current_mode.
endmethod.


METHOD log_msgs_to_bal.

  DATA lt_msg TYPE bal_t_msg.

  TRY.

      CHECK me->_gt_messages IS NOT INITIAL.

      DATA(lo_bal) = NEW zcl_bal( iv_object = 'ZMONITUTIL'
                                  iv_subobject = COND balsubobj( WHEN me->get_current_mode( ) = me->gcs_mode_is-dump THEN 'DUMPS'
                                                                 WHEN me->get_current_mode( ) = me->gcs_mode_is-slg1 THEN 'BAL'
                                                                 WHEN me->get_current_mode( ) = me->gcs_mode_is-idoc THEN 'IDOC' )
                                  iv_extnumber = 'ZMONITORING:' && me->_gv_current_mode_text && '_PROG:' && sy-cprog ).

      lo_bal->add_msg_with_balmsg(
       EXPORTING
        it_bal_t_msg = _gt_messages
      ).

      lo_bal->store_and_free( ).

    CATCH zcx_bal INTO DATA(lo_exp).

  ENDTRY.

ENDMETHOD.
ENDCLASS.
