class ZCL_IDOC_UTILITIES definition
  public
  final
  create public .

public section.

  types:
    tt_bdidocstat TYPE TABLE OF bdidocstat .
  types:
    tt_BDWFRETVAR TYPE TABLE OF bdwfretvar .
  types:
    BEGIN OF ts_mats_for_create_matmas,
        matnr TYPE matnr,
        werks TYPE werks_d,
      END OF ts_mats_for_create_matmas .
  types:
    tt_mats_for_create_matmas TYPE TABLE OF ts_mats_for_create_matmas .

  methods CONSTRUCTOR
    importing
      !IS_CONTROL_DATA type EDIDC
      !IT_IDOC_DATA type EDIDD_TT
      !IV_SIMULATION type SIMKENNZ optional
      !IV_TEST type EDI_TEST optional .
  methods CREATE_OUTBOUND .
  class-methods CREATE_CONTROL_DATA
    importing
      !IV_IDOC_TYPE type EDI_MESTYP
      !IV_IDOC_BASISTYPE type EDI_IDOCTP
      !IV_IDOC_ENHANCEMENT type EDI_CIMTYP optional
      !IV_REC_PARTNERTYPE type EDI_RCVPRT
      !IV_REC_PARTNERROLE type EDI_RCVPFC
      !IV_REC_PARTNERNO type EDI_RCVPRN
      !IV_REC_PORT type EDI_RCVPOR
      !IV_SEN_PARTNERTYPE type EDI_SNDPRT
      !IV_SEN_PARTNERROLE type EDI_SNDPFC
      !IV_SEN_PARTNERNO type EDI_SNDPRN
      !IV_SEN_PORT type EDI_SNDPOR
    returning
      value(RS_CONTROL_DATA) type EDIDC .
  methods CREATE_INBOUND .
  class-methods CREATE_CD_FOR_INB_SIMULATION
    importing
      !IV_IDOC_TYPE type EDI_MESTYP
      !IV_IDOC_BASISTYPE type EDI_IDOCTP
    returning
      value(RS_CONTROL_DATA) type EDIDC .
  class-methods SET_STATUS_VARRETURNS_INB_PROC
    importing
      !IV_DOCNUM type EDI_DOCNUM
      !IS_SUCCESS_MSG type BAL_S_MSG optional
      !IS_FAILED_MSG type BAL_S_MSG optional
    exporting
      !EV_WORKFLOW_RESULT type BDWFAP_PAR-RESULT
    changing
      !XT_STATUS type TT_BDIDOCSTAT
      !XT_RETURN_VARS type TT_BDWFRETVAR .
  methods GET_INST_MESSAGES
    returning
      value(RT_MESSAGES) type BAL_T_MSG .
  class-methods CREATE_MATMAS_DATASETS
    importing
      !IS_MATERIAL type TS_MATS_FOR_CREATE_MATMAS optional
      !IT_MATERIALS type TT_MATS_FOR_CREATE_MATMAS optional
      !IV_E1MARAM_CONVERT_TO type CHAR03 optional
      !IV_E1MARMM_CONVERT_TO type CHAR03 optional
      !IV_NO_MARC_DATA_NO_IDOC type ABAP_BOOL optional
      !IV_NO_MARM_DATA_NO_IDOC type ABAP_BOOL optional
    exporting
      !ET_LOG type BAL_T_MSG
      !ET_MATMAS_EDIDD type EDIDD_TT .
  PROTECTED SECTION.
private section.

  data _GS_CONTROL_DATA type EDIDC .
  data _GT_IDOC_DATA type EDIDD_TT .
  data _GV_TEST type EDI_TEST .
  data _GV_SIMULATION type SIMKENNZ .
  data _GT_INST_MESSAGES type BAL_T_MSG .
  class-data _GS_MESSAGE type BAL_S_MSG .
  constants _GC_MC type MSGID value 'ZMC_IDOC_UTILITIES' ##NO_TEXT.

  methods _ADD_MESSAGE
    importing
      !IV_SY type SYST .
  methods README .
ENDCLASS.



CLASS ZCL_IDOC_UTILITIES IMPLEMENTATION.


  METHOD constructor.
    me->_gs_control_data = is_control_data.
    me->_gt_idoc_data = it_idoc_data.
    me->_gv_test = iv_test.
    me->_gv_simulation = iv_simulation.
  ENDMETHOD.


  METHOD create_cd_for_inb_simulation.
    rs_control_data-sndprt = 'LS'.
    rs_control_data-direct = '2'.
    CONCATENATE sy-sysid 'CLNT' sy-mandt INTO rs_control_data-sndprn.
    CONCATENATE 'SAP_' sy-sysid '_00' INTO rs_control_data-sndpor.
    rs_control_data-rcvprn = rs_control_data-sndprn.
    rs_control_data-rcvprt = rs_control_data-sndprt.
    CONCATENATE 'SAP' sy-sysid INTO rs_control_data-rcvpor.

    rs_control_data-mestyp = iv_idoc_type.
    rs_control_data-idoctp = iv_idoc_basistype.
  ENDMETHOD.


  METHOD create_control_data.
    rs_control_data-mestyp = iv_idoc_type.
    rs_control_data-idoctp = iv_idoc_basistype.
    rs_control_data-cimtyp = iv_idoc_enhancement.
    rs_control_data-rcvprt = iv_rec_partnertype.
    rs_control_data-rcvpfc = iv_rec_partnerrole.
    rs_control_data-rcvprn = iv_rec_partnerno.
    rs_control_data-rcvpor = iv_rec_port.
    rs_control_data-sndprt = iv_sen_partnertype.
    rs_control_data-sndpfc = iv_sen_partnerrole.
    rs_control_data-sndprn = iv_sen_partnerno.
    rs_control_data-sndpor = iv_sen_port.
  ENDMETHOD.


  METHOD create_inbound.
    CALL FUNCTION 'IDOC_INBOUND_WRITE_TO_DB'
      TABLES
        t_data_records    = me->_gt_idoc_data
      CHANGING
        pc_control_record = me->_gs_control_data
      EXCEPTIONS
        idoc_not_saved    = 1
        OTHERS            = 2.
    IF sy-subrc <> 0.
      MESSAGE e005(zmc_idoc_utilities) INTO DATA(ls_msg) WITH _gs_control_data-mestyp _gs_control_data-docnum _gs_control_data-status.
      me->_add_message( EXPORTING iv_sy = sy ).
    ELSE.
      IF me->_gv_simulation = abap_true.
        MESSAGE w007(zmc_idoc_utilities) INTO ls_msg WITH _gs_control_data-mestyp.
        ROLLBACK WORK.
      ELSE.
        COMMIT WORK AND WAIT.
        IF sy-subrc = 0.
          MESSAGE s004(zmc_idoc_utilities) INTO ls_msg WITH _gs_control_data-mestyp _gs_control_data-docnum _gs_control_data-status.
        ELSE.
          MESSAGE e003(zmc_idoc_utilities) INTO ls_msg WITH _gs_control_data-mestyp _gs_control_data-docnum.
        ENDIF.
      ENDIF.
      me->_add_message( EXPORTING iv_sy = sy ).
    ENDIF.
  ENDMETHOD.


  METHOD create_outbound.

    DATA lt_comm_control TYPE TABLE OF edidc.
    FIELD-SYMBOLS <ls_comm_control> TYPE edidc.

    IF me->_gv_test = abap_true. me->_gs_control_data-test = abap_true. ENDIF. "Test flag in IDoc
    CALL FUNCTION 'MASTER_IDOC_DISTRIBUTE'
      EXPORTING
        master_idoc_control            = me->_gs_control_data
*       OBJ_TYPE                       = ''
*       CHNUM                          = ''
      TABLES
        communication_idoc_control     = lt_comm_control
        master_idoc_data               = me->_gt_idoc_data
      EXCEPTIONS
        error_in_idoc_control          = 1
        error_writing_idoc_status      = 2
        error_in_idoc_data             = 3
        sending_logical_system_unknown = 4
        OTHERS                         = 5.

    IF sy-subrc <> 0.
      me->_add_message( EXPORTING iv_sy = sy ).
    ELSE.
      READ TABLE lt_comm_control ASSIGNING FIELD-SYMBOL(<ls_idoc_result>) INDEX 1.
      IF me->_gv_simulation = abap_true.
        MESSAGE w007(zmc_idoc_utilities) INTO DATA(ls_msg) WITH <ls_idoc_result>-mestyp.
        ROLLBACK WORK.
      ELSE.
        COMMIT WORK AND WAIT.
        IF sy-subrc = 0.
          MESSAGE s001(zmc_idoc_utilities) INTO ls_msg WITH <ls_idoc_result>-mestyp <ls_idoc_result>-docnum <ls_idoc_result>-status.
        ELSE.
          MESSAGE e003(zmc_idoc_utilities) INTO ls_msg WITH <ls_idoc_result>-mestyp <ls_idoc_result>-docnum.
        ENDIF.
      ENDIF.
      me->_add_message( EXPORTING iv_sy = sy ).
    ENDIF.
  ENDMETHOD.


METHOD readme.

*==== C R E A T E D  B Y
* Marvin Michel
* say-hi@m1ch3l.de
* wiki.m1ch3l.de

*==== U S A G E IN COMMERCIAL AND PRIVAT ENVIRONMENT
* This z-class was created in freetime and NOT
* in a salaried activity.
* Because of this, the z-class
* can be used without any restriction.

*==== L I N K S
* EXPLANATION IN GERMAN: wiki.m1ch3l.de/ZCL_CONV_UTILITIES
* DONWLOAD: gitlab.com/m1ch3l-de/abap-code-collection/-/tree/master/zcl_conv_utilities

*==== C H A N G E L O G
*-------------------------------------------------------------------------
* yyyymmdd by fname lname:
*   Your changes here ;)
*-------------------------------------------------------------------------
* 20210530 by Marvin Michel in well deserved vacation:
*   New static method "create_matmas_datasets", so that it's
*   possible to create a standard MATMAS with some nice functions:
*    - Optional quantity, weight and volume conversion SAP<->ISO
*      for segment E1MARAM and E1MARMM
*    - Optional no datasets that means no IDoc when there are no data for
*      MARC and MARM
ENDMETHOD.


  METHOD _add_message.
    CLEAR _gs_message.
    MOVE-CORRESPONDING iv_sy TO _gs_message.
    APPEND _gs_message TO me->_gt_inst_messages.
  ENDMETHOD.


  METHOD create_matmas_datasets.

    DATA: lt_materials TYPE tt_mats_for_create_matmas.

    "The material table is preferred if it is filled
    "That means for one IDoc there is more than one material

    "When you want only one material per Idoc
    "deserve only the structure

    CHECK it_materials IS NOT INITIAL OR is_material IS NOT INITIAL.
    IF iv_e1maram_convert_to <> space.
      CHECK iv_e1maram_convert_to = 'SAP' OR iv_e1maram_convert_to = 'ISO'.
    ENDIF.
    IF iv_e1marmm_convert_to <> space.
      CHECK iv_e1marmm_convert_to = 'SAP' OR iv_e1marmm_convert_to = 'ISO'.
    ENDIF.

    IF it_materials IS NOT INITIAL.
      lt_materials[] = it_materials.
    ELSE.
      IF is_material IS NOT INITIAL.
        APPEND INITIAL LINE TO lt_materials ASSIGNING FIELD-SYMBOL(<add_single_mat>).
        <add_single_mat> = is_material.
      ENDIF.
    ENDIF.

    lcl_matmas_helper=>create_matmas_segments( EXPORTING it_materials = lt_materials
                                                                 iv_e1maram_convert_to = iv_e1maram_convert_to
                                                                 iv_e1marmm_convert_to = iv_e1marmm_convert_to
                                                                 iv_no_marc_data_no_idoc = iv_no_marc_data_no_idoc
                                                                 iv_no_marm_data_no_idoc = iv_no_marm_data_no_idoc
                                               IMPORTING et_log = et_log
                                                         et_matmas_edidd = et_matmas_edidd
                                                               ).
  ENDMETHOD.


  METHOD GET_INST_MESSAGES.

    IF lcl_misc_helper=>get_static_messages( ) IS NOT INITIAL.
     APPEND LINES OF lcl_misc_helper=>get_static_messages( ) TO rt_messages.
     lcl_misc_helper=>clear_static_messages( ).
    ENDIF.

    APPEND LINES of me->_gt_inst_messages TO rt_messages.
  ENDMETHOD.


METHOD set_status_varreturns_inb_proc.

  DATA: lv_eid         TYPE bdwfretvar-wf_param VALUE 'Error_IDOCs',
        lv_pid         TYPE bdwfretvar-wf_param VALUE 'Processed_IDOCs',
        lv_apo         TYPE bdwfretvar-wf_param VALUE 'Appl_Objects',
        ls_idoc_status LIKE LINE OF xt_status,
        ls_return_Vars LIKE LINE OF xt_return_vars.

  CONSTANTS: lc_status_51_failed     TYPE edidc-status VALUE '51',
             lc_status_53_successful TYPE edidc-status VALUE '53',
             lc_wf_result_error      TYPE bdwfap_par-result VALUE '99999',
             lc_wf_result_ok         TYPE bdwfap_par-result VALUE '0',
             lc_eid                  TYPE bdwfretvar-wf_param VALUE 'Error_IDOCs',
             lc_pid                  TYPE bdwfretvar-wf_param VALUE 'Processed_IDOCs',
             lc_apo                  TYPE bdwfretvar-wf_param VALUE 'Appl_Objects'.

  IF is_failed_msg IS NOT INITIAL AND is_failed_msg IS SUPPLIED.

    ls_idoc_status-docnum = iv_docnum.
    ls_idoc_status-status = lc_status_51_failed.
    MOVE-CORRESPONDING is_failed_msg TO ls_idoc_status.
    APPEND ls_idoc_status TO xt_status.

    ev_workflow_result = lc_wf_result_error.

    ls_return_vars-wf_param   = lc_eid.
    ls_return_vars-doc_number = iv_docnum.
    APPEND ls_idoc_status TO xt_return_vars.

  ELSEIF is_success_msg IS NOT INITIAL AND is_success_msg IS SUPPLIED. "OK

    ls_idoc_status-docnum = iv_docnum.
    ls_idoc_status-status = lc_status_53_successful.
    MOVE-CORRESPONDING is_success_msg TO ls_idoc_status.
    APPEND ls_idoc_status TO xt_status.

    ev_workflow_result = lc_wf_result_ok.

    ls_return_vars-wf_param   = lc_pid.
    ls_return_vars-doc_number = iv_docnum.
    APPEND ls_return_vars TO xt_return_vars.

    ls_return_vars-wf_param   = lc_apo.
    ls_return_vars-doc_number = iv_docnum.
    APPEND ls_return_vars TO xt_return_vars.

  ENDIF.

ENDMETHOD.
ENDCLASS.
