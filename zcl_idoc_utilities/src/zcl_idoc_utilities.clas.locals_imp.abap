CLASS lcl_misc_helper DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS:
      add_static_message,
      get_static_messages RETURNING VALUE(rt_messages) TYPE bal_t_msg,
      clear_static_messages.

  PRIVATE SECTION.

    CLASS-DATA:
      _gt_static_messages TYPE bal_t_msg,
      _gs_message         TYPE  bal_s_msg.

ENDCLASS.

CLASS lcl_general_helper DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS:
      clear_segnum,
      get_segnum
        RETURNING VALUE(rv_segnum) TYPE idocdsgnum,
      set_segnum
        IMPORTING iv_new_root      TYPE flag OPTIONAL
        RETURNING VALUE(rv_segnum) TYPE idocdsgnum,
      get_segnum_root
        IMPORTING iv_new_root           TYPE flag OPTIONAL
        RETURNING VALUE(rv_segnum_root) TYPE idocdsgnum,
      set_segnum_start_loop
        IMPORTING iv_start_segnum TYPE idocdsgnum,
      get_segnum_start_loop
        RETURNING VALUE(rv_segnum_start_loop) TYPE idocdsgnum.

  PRIVATE SECTION.

    CLASS-DATA:
      _segnum_root_counter TYPE idocdsgnum VALUE 1,
      _segnum_counter      TYPE idocdsgnum VALUE 1,
      _segnum_start_loop   TYPE idocdsgnum.

ENDCLASS.

CLASS lcl_matmas_helper DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS:
      create_matmas_segments
        IMPORTING it_materials            TYPE zcl_idoc_utilities=>tt_mats_for_create_matmas
                  iv_e1maram_convert_to   TYPE char03 OPTIONAL
                  iv_e1marmm_convert_to   TYPE char03 OPTIONAL
                  iv_no_marc_data_no_idoc TYPE abap_bool OPTIONAL
                  iv_no_marm_data_no_idoc TYPE abap_bool OPTIONAL
        EXPORTING et_matmas_edidd  TYPE edidd_tt
                  et_log TYPE bal_t_msg.

  PRIVATE SECTION.
    CLASS-METHODS:
      _e1maram_convert_units
        IMPORTING
          iv_conversion_to    TYPE char03
          iv_convert_quantity TYPE abap_bool OPTIONAL
          iv_convert_weight   TYPE abap_bool OPTIONAL
          iv_convert_volume   TYPE abap_bool OPTIONAL
        CHANGING
          cs_e1maram          TYPE e1maram
        RAISING
          zcx_idoc_utilities,

      _e1marmm_convert_units
        IMPORTING
          iv_conversion_to       TYPE char03
          !iv_convert_quantity   TYPE abap_bool OPTIONAL
          iv_convert_weight      TYPE abap_bool OPTIONAL
          iv_convert_volume      TYPE abap_bool OPTIONAL
          iv_convert_len_wid_hei TYPE abap_bool OPTIONAL
        CHANGING
          !cs_e1marmm            TYPE e1marmm
        RAISING
          zcx_idoc_utilities.

    CONSTANTS: cv_e1maram TYPE string VALUE 'E1MARAM',
               cv_e1maktm TYPE e1maktm VALUE 'E1MAKTM',
               cv_e1marcm TYPE e1marcm VALUE 'E1MARCM',
               cv_e1marmm TYPE e1marmm VALUE 'E1MARMM'.

ENDCLASS.

CLASS lcl_misc_helper IMPLEMENTATION.

  METHOD add_static_message.
    CLEAR lcl_misc_helper=>_gs_message.
    MOVE-CORRESPONDING sy TO lcl_misc_helper=>_gs_message.
    APPEND lcl_misc_helper=>_gs_message TO lcl_misc_helper=>_gt_static_messages.
  ENDMETHOD.

  METHOD get_static_messages.
    rt_messages = lcl_misc_helper=>_gt_static_messages.
  ENDMETHOD.

  METHOD clear_static_messages.
    CLEAR lcl_misc_helper=>_gt_static_messages.
  ENDMETHOD.

ENDCLASS.

CLASS lcl_general_helper IMPLEMENTATION.

  METHOD clear_segnum.
    CLEAR: lcl_general_helper=>_segnum_counter,
           lcl_general_helper=>_segnum_root_counter.
  ENDMETHOD.

  METHOD get_segnum_root.
    rv_segnum_root = lcl_general_helper=>_segnum_root_counter.
  ENDMETHOD.

  METHOD get_segnum.
    rv_segnum = lcl_general_helper=>_segnum_counter.
  ENDMETHOD.

  METHOD set_segnum.
    lcl_general_helper=>_segnum_counter += 1.
    rv_segnum = lcl_general_helper=>_segnum_counter.
    CHECK iv_new_root = abap_true.
    lcl_general_helper=>_segnum_root_counter = lcl_general_helper=>_segnum_counter.
  ENDMETHOD.

  METHOD set_segnum_start_loop.
    lcl_general_helper=>_segnum_start_loop = iv_start_segnum.
  ENDMETHOD.

  METHOD get_segnum_start_loop.
    rv_segnum_start_loop = lcl_general_helper=>_segnum_start_loop.
  ENDMETHOD.

ENDCLASS.

CLASS lcl_matmas_helper IMPLEMENTATION.

  METHOD _e1maram_convert_units.

    DATA: lv_do   TYPE string,
          lv_iso  TYPE isocd_unit,
          lv_sap  TYPE msehi,
          lv_conv TYPE i.

    CHECK iv_convert_quantity = abap_true OR iv_convert_weight = abap_true OR iv_convert_volume = abap_true.

    IF iv_convert_quantity = abap_true. lv_do = 'Q'. ENDIF.
    IF iv_convert_weight = abap_true. lv_do = lv_do && 'W'. ENDIF.
    IF iv_convert_volume = abap_true. lv_do = lv_do && 'V'. ENDIF.

    DO strlen( lv_do ) TIMES.

      CLEAR: lv_sap, lv_iso.
      DATA(lv_current_conversion) = lv_do+lv_conv(1).

      CASE iv_conversion_to.

        WHEN 'SAP'.

          CASE lv_current_conversion.
            WHEN 'Q'.
              lv_iso = cs_e1maram-meins.
            WHEN 'W'.
              lv_iso = cs_e1maram-gewei.
            WHEN 'V'.
              lv_iso = cs_e1maram-voleh.
          ENDCASE.

          CALL FUNCTION 'UNIT_OF_MEASURE_ISO_TO_SAP'
            EXPORTING
              iso_code  = lv_iso
            IMPORTING
              sap_code  = lv_sap
            EXCEPTIONS
              not_found = 1
              OTHERS    = 2.
          IF sy-subrc = 0.
            CASE lv_current_conversion.
              WHEN 'Q'.
                cs_e1maram-meins = lv_sap.
              WHEN 'W'.
                cs_e1maram-gewei = lv_sap.
              WHEN 'V'.
                cs_e1maram-voleh = lv_sap.
            ENDCASE.
            MESSAGE s012(zmc_idoc_utilities) INTO DATA(ls_conv_iso_sap_suc) WITH lv_current_conversion lv_iso lv_sap.
            lcl_misc_helper=>add_static_message( ).
          ELSE.
            MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4 INTO DATA(ls_fuba_msg).
            lcl_misc_helper=>add_static_message( ).
          ENDIF.

        WHEN 'ISO'.

          CASE lv_current_conversion.
            WHEN 'Q'.
              lv_sap = cs_e1maram-meins.
            WHEN 'W'.
              lv_sap = cs_e1maram-gewei.
            WHEN 'V'.
              lv_sap = cs_e1maram-voleh.
          ENDCASE.

          CALL FUNCTION 'UNIT_OF_MEASURE_SAP_TO_ISO'
            EXPORTING
              sap_code    = lv_sap
            IMPORTING
              iso_code    = lv_iso
            EXCEPTIONS
              not_found   = 1
              no_iso_code = 2
              OTHERS      = 3.
          IF sy-subrc    = 0.
            CASE lv_current_conversion.
              WHEN 'Q'.
                cs_e1maram-meins = lv_iso.
              WHEN 'W'.
                cs_e1maram-gewei = lv_iso.
              WHEN 'V'.
                cs_e1maram-voleh = lv_iso.
            ENDCASE.
            MESSAGE s012(zmc_idoc_utilities) INTO DATA(ls_conv_sap_iso_suc) WITH lv_current_conversion lv_sap lv_iso.
            lcl_misc_helper=>add_static_message( ).
          ELSE.
            MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4 INTO DATA(ls_fuba_msg2).
            lcl_misc_helper=>add_static_message( ).
          ENDIF.

        WHEN OTHERS.
          RAISE EXCEPTION TYPE zcx_idoc_utilities
             MESSAGE e000(zmc_idoc_utilities) WITH 'only "SAP" or "ISO" allowed'.

      ENDCASE.

      lv_conv += 1.
    ENDDO.

  ENDMETHOD.


* <SIGNATURE>---------------------------------------------------------------------------------------+
* | Static Public Method ZCL_IDOC_UTILITIES=>E1MARMM_CONVERT_UNITS
* +-------------------------------------------------------------------------------------------------+
* | [--->] IV_CONVERSION_TO               TYPE        CHAR03
* | [--->] IV_CONVERT_QUANTITY            TYPE        ABAP_BOOL(optional)
* | [--->] IV_CONVERT_WEIGHT              TYPE        ABAP_BOOL(optional)
* | [--->] IV_CONVERT_VOLUME              TYPE        ABAP_BOOL(optional)
* | [--->] IV_CONVERT_LEN_WID_HEI         TYPE        ABAP_BOOL
* | [<-->] CS_E1MARMM                     TYPE        E1MARMM
* | [!CX!] ZCX_IDOC_UTILITIES
* +--------------------------------------------------------------------------------------</SIGNATURE>
  METHOD _e1marmm_convert_units.

    DATA: lv_do   TYPE string,
          lv_iso  TYPE isocd_unit,
          lv_sap  TYPE msehi,
          lv_conv TYPE i.

    CHECK iv_convert_quantity = abap_true OR iv_convert_weight = abap_true OR iv_convert_volume = abap_true.

    IF iv_convert_quantity = abap_true. lv_do = 'Q'. ENDIF.
    IF iv_convert_weight = abap_true. lv_do = lv_do && 'W'. ENDIF.
    IF iv_convert_volume = abap_true. lv_do = lv_do && 'V'. ENDIF.
    IF iv_convert_volume = abap_true. lv_do = lv_do && 'M'. ENDIF.

    DO strlen( lv_do ) TIMES.

      CLEAR: lv_sap, lv_iso.
      DATA(lv_current_conversion) = lv_do+lv_conv(1).

      CASE iv_conversion_to.

        WHEN 'SAP'.

          CASE lv_current_conversion.
            WHEN 'Q'.
              lv_iso = cs_e1marmm-meinh.
            WHEN 'W'.
              lv_iso = cs_e1marmm-gewei.
            WHEN 'V'.
              lv_iso = cs_e1marmm-voleh.
            WHEN 'M'.
              lv_iso = cs_e1marmm-meabm.
          ENDCASE.

          CALL FUNCTION 'UNIT_OF_MEASURE_ISO_TO_SAP'
            EXPORTING
              iso_code  = lv_iso
            IMPORTING
              sap_code  = lv_sap
            EXCEPTIONS
              not_found = 1
              OTHERS    = 2.
          IF sy-subrc = 0.
            CASE lv_current_conversion.
              WHEN 'Q'.
                cs_e1marmm-meinh = lv_sap.
              WHEN 'W'.
                cs_e1marmm-gewei = lv_sap.
              WHEN 'V'.
                cs_e1marmm-voleh = lv_sap.
              WHEN 'M'.
                cs_e1marmm-meabm = lv_sap.
            ENDCASE.
            MESSAGE s012(zmc_idoc_utilities) INTO DATA(ls_conv_iso_sap_suc) WITH lv_current_conversion lv_iso lv_sap .
            lcl_misc_helper=>add_static_message( ).
          ELSE.
            MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4 INTO DATA(ls_fuba_msg).
            lcl_misc_helper=>add_static_message( ).
          ENDIF.

        WHEN 'ISO'.

          CASE lv_current_conversion.
            WHEN 'Q'.
              lv_sap = cs_e1marmm-meinh.
            WHEN 'W'.
              lv_sap = cs_e1marmm-gewei.
            WHEN 'V'.
              lv_sap = cs_e1marmm-voleh.
            WHEN 'M'.
              lv_sap = cs_e1marmm-meabm.
          ENDCASE.

          CALL FUNCTION 'UNIT_OF_MEASURE_SAP_TO_ISO'
            EXPORTING
              sap_code    = lv_sap
            IMPORTING
              iso_code    = lv_iso
            EXCEPTIONS
              not_found   = 1
              no_iso_code = 2
              OTHERS      = 3.
          IF sy-subrc    = 0.
            CASE lv_current_conversion.
              WHEN 'Q'.
                cs_e1marmm-meinh = lv_iso.
              WHEN 'W'.
                cs_e1marmm-gewei = lv_iso.
              WHEN 'V'.
                cs_e1marmm-voleh = lv_iso.
              WHEN 'M'.
                cs_e1marmm-meabm = lv_iso.
            ENDCASE.
            MESSAGE s012(zmc_idoc_utilities) INTO DATA(ls_conv_sap_iso_suc) WITH lv_current_conversion lv_sap lv_iso.
            lcl_misc_helper=>add_static_message( ).
          ELSE.
            MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4 INTO DATA(ls_fuba_msg2).
            lcl_misc_helper=>add_static_message( ).
          ENDIF.

        WHEN OTHERS.
          RAISE EXCEPTION TYPE zcx_idoc_utilities
           MESSAGE e000(zmc_idoc_utilities) WITH 'only "SAP" or "ISO" allowed'.

      ENDCASE.

      lv_conv += 1.
    ENDDO.

  ENDMETHOD.

  METHOD create_matmas_segments.

    DATA: ls_e1maram       TYPE e1maram,
          ls_e1maktm       TYPE e1maktm,
          ls_e1marcm       TYPE e1marcm,
          ls_e1marmm       TYPE e1marmm,
          lt_idoc_datasets TYPE edidd_tt.

    lcl_misc_helper=>clear_static_messages( ).
    lcl_general_helper=>clear_segnum( ).

    LOOP AT it_materials ASSIGNING FIELD-SYMBOL(<ls_mat>).

      MESSAGE i008(zmc_idoc_utilities) INTO DATA(ls_start_prot) WITH 'MATMAS' <ls_mat>-matnr.
      lcl_misc_helper=>add_static_message( ).

*     ===== E1MARAM =====
      MESSAGE i010(zmc_idoc_utilities) INTO DATA(ls_maram_prot) WITH lcl_matmas_helper=>cv_e1maram.
      lcl_misc_helper=>add_static_message( ).

      APPEND INITIAL LINE TO et_matmas_edidd ASSIGNING FIELD-SYMBOL(<add_idoc_datasets_mara>).
      SELECT SINGLE * FROM mara INTO @DATA(ls_mara) WHERE matnr = @<ls_mat>-matnr.
      MOVE-CORRESPONDING ls_mara TO ls_e1maram.
      IF iv_e1maram_convert_to = 'SAP' OR iv_e1maram_convert_to = 'ISO'.
        lcl_matmas_helper=>_e1maram_convert_units(  EXPORTING iv_conversion_to = iv_e1maram_convert_to
                                                             iv_convert_quantity = abap_true
                                                             iv_convert_volume = abap_true
                                                             iv_convert_weight = abap_true
                                                    CHANGING cs_e1maram = ls_e1maram ).
      ENDIF.
      <add_idoc_datasets_mara>-mandt = sy-mandt.
      <add_idoc_datasets_mara>-sdata = ls_e1maram.
      <add_idoc_datasets_mara>-segnam = 'E1MARAM'.
      <add_idoc_datasets_mara>-segnum = lcl_general_helper=>set_segnum( iv_new_root = abap_true ).
      <add_idoc_datasets_mara>-hlevel = 2.

*     ===== E1MAKTM =====
      MESSAGE i010(zmc_idoc_utilities) INTO DATA(ls_maktm_prot) WITH lcl_matmas_helper=>cv_e1maktm.
      lcl_misc_helper=>add_static_message( ).

      SELECT * FROM makt INTO TABLE @DATA(lt_makt) WHERE matnr = @<ls_mat>-matnr.
      LOOP AT lt_makt ASSIGNING FIELD-SYMBOL(<ls_makt>).
        APPEND INITIAL LINE TO et_matmas_edidd ASSIGNING FIELD-SYMBOL(<add_idoc_datasets_makt>).
        MOVE-CORRESPONDING <ls_makt> TO ls_e1maktm.
        <add_idoc_datasets_makt>-mandt = sy-mandt.
        <add_idoc_datasets_makt>-sdata = ls_e1maktm.
        <add_idoc_datasets_makt>-segnam = 'E1MAKTM'.
        <add_idoc_datasets_makt>-segnum = lcl_general_helper=>set_segnum( ).
        <add_idoc_datasets_makt>-psgnum = lcl_general_helper=>get_segnum_root( ).
        <add_idoc_datasets_makt>-hlevel = 3.
      ENDLOOP.

*     ===== E1MARCM =====
      MESSAGE i010(zmc_idoc_utilities) INTO DATA(ls_marcm_prot) WITH lcl_matmas_helper=>cv_e1marcm.
      lcl_misc_helper=>add_static_message( ).

      SELECT SINGLE * FROM marc INTO @DATA(ls_marc) WHERE matnr = @<ls_mat>-matnr AND werks = @<ls_mat>-werks.
      IF sy-subrc = 0.
        APPEND INITIAL LINE TO et_matmas_edidd ASSIGNING FIELD-SYMBOL(<add_idoc_datasets_marc>).
        MOVE-CORRESPONDING ls_marc TO ls_e1marcm.
        <add_idoc_datasets_marc>-mandt = sy-mandt.
        <add_idoc_datasets_marc>-sdata = ls_e1marcm.
        <add_idoc_datasets_marc>-segnam = 'E1MARCM'.
        <add_idoc_datasets_marc>-segnum = lcl_general_helper=>set_segnum( ).
        <add_idoc_datasets_marc>-psgnum = lcl_general_helper=>get_segnum_root( ).
        <add_idoc_datasets_marc>-hlevel = 3.
      ELSE. "no marc data found
        IF iv_no_marc_data_no_idoc = abap_true.
          DELETE et_matmas_edidd WHERE segnum BETWEEN lcl_general_helper=>get_segnum_root( ) AND
                                       lcl_general_helper=>get_segnum( ).
          MESSAGE w011(zmc_idoc_utilities) INTO DATA(ls_no_marc_data) WITH 'MARC'.
          lcl_misc_helper=>add_static_message( ).
          CONTINUE.
        ENDIF.
      ENDIF.

*     ===== E1MARMM =====
      MESSAGE i010(zmc_idoc_utilities) INTO DATA(ls_marmm_prot) WITH lcl_matmas_helper=>cv_e1marmm.
      lcl_misc_helper=>add_static_message( ).

      SELECT * FROM marm WHERE matnr = @<ls_mat>-matnr INTO TABLE @DATA(lt_marm).
      IF sy-subrc = 0.
        LOOP AT lt_marm ASSIGNING FIELD-SYMBOL(<ls_marm>).
          APPEND INITIAL LINE TO et_matmas_edidd ASSIGNING FIELD-SYMBOL(<add_idoc_datasets_marm>).
          MOVE-CORRESPONDING <ls_marm> TO ls_e1marmm.
          IF iv_e1marmm_convert_to = 'SAP' OR iv_e1marmm_convert_to = 'ISO'.
            lcl_matmas_helper=>_e1marmm_convert_units( EXPORTING iv_conversion_to = iv_e1maram_convert_to
                                                                iv_convert_quantity = abap_true
                                                                iv_convert_volume = abap_true
                                                                iv_convert_weight = abap_true
                                                                iv_convert_len_wid_hei = abap_true
                                                       CHANGING cs_e1marmm = ls_e1marmm ).
          ENDIF.
          <add_idoc_datasets_marm>-mandt = sy-mandt.
          <add_idoc_datasets_marm>-sdata = ls_e1marmm.
          <add_idoc_datasets_marm>-segnam = 'E1MARMM'.
          <add_idoc_datasets_marm>-segnum = lcl_general_helper=>set_segnum( ).
          <add_idoc_datasets_marm>-psgnum = lcl_general_helper=>get_segnum_root( ).
          <add_idoc_datasets_marm>-hlevel = 3.
        ENDLOOP.
      ELSE. "no marm data found
        IF iv_no_marm_data_no_idoc = abap_true.
          DELETE et_matmas_edidd WHERE segnum BETWEEN lcl_general_helper=>get_segnum_root( ) AND
                                       lcl_general_helper=>get_segnum( ).
          MESSAGE w011(zmc_idoc_utilities) INTO DATA(ls_no_marm_data) WITH 'MARM'.
          lcl_misc_helper=>add_static_message( ).
          CONTINUE.
        ENDIF.
      ENDIF.

      MESSAGE i009(zmc_idoc_utilities) INTO DATA(ls_end_prot) WITH 'MATMAS' <ls_mat>-matnr.
      lcl_misc_helper=>add_static_message( ).
    ENDLOOP.

    et_log = lcl_misc_helper=>get_static_messages( ).

  ENDMETHOD.

ENDCLASS.
