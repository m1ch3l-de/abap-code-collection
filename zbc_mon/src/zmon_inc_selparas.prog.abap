*&---------------------------------------------------------------------*
*& Include          ZMON_INC_SELPARAS
*&---------------------------------------------------------------------*

TABLES EDIDS.

SELECTION-SCREEN BEGIN OF BLOCK choose_modus WITH FRAME TITLE TEXT-000.
  PARAMETERS: p_mode TYPE char15 AS LISTBOX VISIBLE LENGTH 20 USER-COMMAND lb_cmd.
SELECTION-SCREEN END OF BLOCK choose_modus.

SELECTION-SCREEN BEGIN OF BLOCK modus_parameters WITH FRAME TITLE TEXT-001.
  PARAMETERS: p_dudate TYPE sy-datum DEFAULT sy-datum MODIF ID dmp,
              p_slobje TYPE balhdr-object MODIF ID bal,
              p_slsubo TYPE balhdr-subobject MODIF ID bal,
              p_sldafr TYPE balhdr-aldate DEFAULT sy-datum MODIF ID bal,
              p_sldato TYPE balhdr-aldate DEFAULT sy-datum MODIF ID bal,
              p_sltifr TYPE balhdr-altime DEFAULT '000000' MODIF ID bal,
              p_sltito TYPE balhdr-altime DEFAULT '235959' MODIF ID bal,
              p_slprgn TYPE balhdr-alprog MODIF ID bal,
              p_slgtco TYPE balhdr-altcode MODIF ID bal,
              p_iddage TYPE edi_logdat DEFAULT sy-datum MODIF ID ido,
              p_idtige TYPE edi_logtim MODIF ID ido.
  SELECT-OPTIONS: s_idstat FOR EDIDS-status MODIF ID ido,
                  s_idtype FOR EDIDS-STATYP MODIF ID ido.
SELECTION-SCREEN END OF BLOCK modus_parameters.

SELECTION-SCREEN BEGIN OF BLOCK choose_func WITH FRAME TITLE TEXT-002.
  PARAMETERS: p_func TYPE char25 AS LISTBOX VISIBLE LENGTH 20 MODIF ID fun USER-COMMAND lb_cmd,
              p_dist TYPE so_obj_nam MODIF ID dis,
              p_dir  TYPE string LOWER CASE MODIF ID dir,
              p_protoc AS CHECKBOX MODIF ID pro.
SELECTION-SCREEN END OF BLOCK choose_func.
