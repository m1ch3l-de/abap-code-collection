*&---------------------------------------------------------------------*
*& Include          ZMON_INC_LCL
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*& Class LCL
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
CLASS lcl DEFINITION FINAL.
  PUBLIC SECTION.
    CLASS-METHODS initialization CHANGING ct_cbval TYPE vrm_values
                                          ct_func  TYPE vrm_values
                                          cv_mode  TYPE char15
                                          cv_func  TYPE char25.
    CLASS-METHODS select_screen IMPORTING iv_mode TYPE char15
                                          iv_func TYPE char25
                                          iv_dist TYPE so_obj_nam
                                          iv_dir  TYPE string
                                          iv_prot TYPE abap_bool.
ENDCLASS.
*&---------------------------------------------------------------------*
*& Class (Implementation) LCL
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
CLASS lcl IMPLEMENTATION.

  METHOD initialization.

  IF sy-batch = abap_false.
    DATA(picture) = NEW cl_gui_picture( parent = NEW cl_gui_gos_container( width = 39 ) ).
    picture->load_picture_from_url_async( url = 'https://wiki.m1ch3l.de/images/favicon-16x16.png' ).
    picture->set_display_mode( picture->display_mode_fit ).
 ENDIF.

    ct_cbval = VALUE #( ( key = 'DUMP' text = 'DUMP' )
                        ( key = 'BAL' text = 'BAL' )
                        ( key = 'IDOC' text = 'IDOC' ) ).

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = 'P_MODE'
        values          = ct_cbval
      EXCEPTIONS
        id_illegal_name = 1
        OTHERS          = 2.

    IF sy-subrc = 0.
      IF lines( ct_cbval ) > 0.
        "Vorselektion 'Punkt eins'
        cv_mode = 'CHOOSE MODE...'.
      ENDIF.
    ENDIF.

    ct_func = VALUE #( ( key = 'GUI' text = 'Show on GUI' )
                    ( key = 'MAIL' text = 'Send via E-Mail' )
                    ( key = 'APPSERVER' text = 'Save on Applicationserver' ) ).

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = 'P_FUNC'
        values          = ct_func
      EXCEPTIONS
        id_illegal_name = 1
        OTHERS          = 2.

    IF sy-subrc = 0.
      IF lines( ct_func ) > 0.
        "Vorselektion 'Punkt eins'
        cv_func = 'CHOOSE FUNCTION...'.
      ENDIF.
    ENDIF.

  ENDMETHOD.

  METHOD select_screen.

    LOOP AT SCREEN.

      IF iv_mode <> 'DUMP' AND screen-group1 = 'DMP'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF iv_mode <> 'BAL' AND screen-group1 = 'BAL'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF iv_mode <> 'IDOC' AND screen-group1 = 'IDO'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF ( iv_mode = 'CHOOSE MODE...' OR iv_mode = '') AND screen-group1 = 'FUN'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF ( iv_func = 'CHOOSE FUNCTION...' OR iv_func = '' OR iv_func = 'GUI') AND screen-group1 = 'PRO'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF ( iv_mode = 'CHOOSE MODE...' OR iv_mode = '') AND screen-group1 = 'PRO'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF iv_func <> 'MAIL' AND screen-group1 = 'DIS'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      IF iv_func <> 'APPSERVER' AND screen-group1 = 'DIR'.
        screen-input = 0.
        screen-invisible = 1.
      ENDIF.

      MODIFY SCREEN.

    ENDLOOP.

   IF iv_func = 'MAIL' AND iv_dist IS INITIAL.
    MESSAGE 'Choose a distribution list and execute the program' TYPE 'S'.
   ENDIF.

   IF iv_func = 'APPSERVER' AND iv_dir IS INITIAL.
    MESSAGE 'Choose a path and execute the program' TYPE 'S'.
   ENDIF.

  ENDMETHOD.
ENDCLASS.
