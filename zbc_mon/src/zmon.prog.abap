*&---------------------------------------------------------------------*
*& Report ZMON
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zmon.

INCLUDE zmon_inc_lcl.
DATA: gt_cbval     TYPE vrm_values,
      gt_func      TYPE vrm_values,
      gv_curr_func TYPE char15,
      gt_idoc_status TYPE zcl_monitoring_utilities=>tt_range_idoc_status,
      gt_idoc_status_type TYPE zcl_monitoring_utilities=>tt_range_idoc_statustype.
INCLUDE zmon_inc_selparas.

AT SELECTION-SCREEN OUTPUT.
  lcl=>select_screen( EXPORTING iv_mode = p_mode
                                iv_func = p_func
                                iv_dist = p_dist
                                iv_dir  = p_dir
                                iv_prot = p_protoc  ).

INITIALIZATION.
  lcl=>initialization( CHANGING ct_cbval = gt_cbval cv_mode = p_mode ct_func = gt_func cv_func = p_func ).

START-OF-SELECTION.

  TRY.
      APPEND LINES OF s_idstat TO gt_idoc_status.
      APPEND LINES OF s_idtype TO gt_idoc_status_type.

      DATA(go_mode) = NEW zcl_monitoring_utilities(
        iv_set_monitoring    = COND #( WHEN p_mode = 'DUMP' THEN zcl_monitoring_utilities=>gcs_mode_is-dump
                                       WHEN p_mode = 'BAL'  THEN zcl_monitoring_utilities=>gcs_mode_is-slg1
                                       WHEN p_mode = 'IDOC' THEN zcl_monitoring_utilities=>gcs_mode_is-idoc )
        iv_dumps_date        = p_dudate
        iv_slg1_object       = p_slobje
        iv_slg1_subobject    = p_slsubo
        iv_slg1_date_from    = p_sldafr
        iv_slg1_date_to      = p_sldato
        iv_slg1_time_from    = p_sltifr
        iv_slg1_time_to      = p_sltifr
        iv_slg1_programname  = p_slprgn
        iv_slg1_tcode        = p_slgtco
        iv_idoc_logdate_ge   = p_iddage
        iv_idoc_logtime_ge   = p_idtige
        it_idoc_status_range = gt_idoc_status
        it_idoc_sttype_range = gt_idoc_status_type
      ).

      IF p_func = 'GUI'.
        go_mode->show_on_gui( ).
      ELSEIF p_func = 'MAIL' AND p_dist IS NOT INITIAL.
        go_mode->send_via_email( iv_distributionlist = p_dist ).
      ELSEIF p_func = 'APPSERVER' AND p_dist IS INITIAL AND p_dir IS NOT INITIAL.
        go_mode->save_on_applserver( iv_path_name_to_applserver = p_dir ).
      ENDIF.

      IF p_protoc = abap_true.
       go_mode->log_msgs_to_bal( ).
      ENDIF.

    CATCH zcx_monitoring_utilities INTO DATA(zcx_mon).
     IF p_protoc = abap_false.
      cl_demo_output=>display_data( EXPORTING name = go_mode->get_current_mode( ) && 'OUTPUT' value = go_mode->get_messages( ) ).
     ENDIF.
     MESSAGE e000(zmc_monti_utilities) INTO DATA(lv_unknown_error) WITH 'MODE' go_mode->get_current_mode( ) 'UNKNOWN ERROR' 'SEE OUTPUT OR BAL/SLG1'.
  ENDTRY.
